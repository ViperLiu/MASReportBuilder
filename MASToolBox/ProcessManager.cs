﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MASToolBox
{
    class ProcessManager
    {
        public static List<Process> ProcessList = new List<Process>();

        public static List<int> ProcessId = new List<int>();
        
        public static void TerminateAllProcess()
        {
            foreach(var p in ProcessList)
            {
                try
                {
                    if (p.ProcessName == "conhost")
                        continue;
                    p.Kill();
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static void KillProcess(int pid)
        {
            // Cannot close 'system idle process'.
            if (pid == 0)
            {
                return;
            }
            ManagementObjectSearcher searcher = new ManagementObjectSearcher
              ("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                KillProcess(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                var procName = proc.ProcessName;
                Console.WriteLine("Killed Proc : " + pid + " : " + procName);
                proc.Kill();
            }
            catch (Exception e)
            {
                // Process already exited.
                Console.WriteLine(e.Message);
                Console.WriteLine("==========");
            }
        }

        public static void ListProcess(int pid)
        {
            // Cannot close 'system idle process'.
            if (pid == 0)
            {
                return;
            }
            ManagementObjectSearcher searcher = new ManagementObjectSearcher
              ("Select * From Win32_Process Where ParentProcessID=" + pid);
            ManagementObjectCollection moc = searcher.Get();
            foreach (ManagementObject mo in moc)
            {
                ListProcess(Convert.ToInt32(mo["ProcessID"]));
            }
            try
            {
                Process proc = Process.GetProcessById(pid);
                var procName = proc.ProcessName;
                if (!ProcessId.Contains(proc.Id) && proc.Id != Process.GetCurrentProcess().Id)
                {
                    ProcessList.Add(proc);
                    ProcessId.Add(proc.Id);
                }
                //Console.WriteLine(pid + " : " + procName);
            }
            catch
            {
            }
        }

        public static void TrackAllProcess()
        {
            Process p = Process.GetCurrentProcess();
            while (true)
            {
                ListProcess(p.Id);
                Thread.Sleep(300);
            }
        }
    }
}
