﻿using System.IO;
using System.Windows.Forms;

namespace MASToolBox
{
    partial class Form1
    {
        private void TabControl1_Selected(object sender, TabControlEventArgs e)
        {
            TabControl tmp = (TabControl)sender;
            if (tmp.SelectedTab == tmp.TabPages["tab_MobSF"])
            {
                this.LoadMobSFSettings();
            }
            else if(tmp.SelectedTab == tmp.TabPages["tab_ADB"])
            {
                if(!FirstFoundPhone)
                    ADBCheckPhone();
            }
        }

        private void TxtFolderPath_DragDrop(object sender, DragEventArgs e)
        {
            var file = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            var extension = Path.GetExtension(file).ToLower();
            if (extension == ".ipa")
            {
                tbOutput.AppendText("ipa無法反組譯\r\n");
                btn_decompile.Enabled = false;
                btn_sendToMobSF.Enabled = false;
                this.tbOutputDir.Enabled = false;
                this.tbOutputDir.Text = "";
            }
            else if (extension == ".apk")
            {
                btn_decompile.Enabled = true;
                btn_sendToMobSF.Enabled = true;
                this.tbOutputDir.Enabled = true;
            }
            this.textBox1.Text = file;
            this.tbOutputDir.Text = textBox1.Text + ".exctracted";
            tbSHA1.Text = GetSHA1();
            tbMD5.Text = GetMD5();
        }

        private void TxtFolderPath_DragEnter(object sender, DragEventArgs e)
        {
            var file = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            var extension = Path.GetExtension(file);
            extension = extension.ToLower();
            if (extension == ".apk" || extension == ".ipa")
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void Tb_Folder_DragEnter(object sender, DragEventArgs e)
        {
            var path = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            if (Directory.Exists(path))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void Tb_Folder_DragDrop(object sender, DragEventArgs e)
        {
            var textBox = sender as TextBox;
            var path = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            textBox.Text = path;
        }
    }
}
