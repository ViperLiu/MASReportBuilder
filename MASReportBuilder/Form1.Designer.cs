﻿namespace MASReportBuilder
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("4.1.1.1.2");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("4.1.1.1", new System.Windows.Forms.TreeNode[] {
            treeNode1});
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("4.1.1.3.1");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("4.1.1.3", new System.Windows.Forms.TreeNode[] {
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("4.1.2.1.1");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("4.1.2.1.2");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("4.1.2.1", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6});
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("4.1.2.3.1");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("4.1.2.3.2");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("4.1.2.3.4");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("4.1.2.3.5");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("4.1.2.3.6");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("4.1.2.3.7");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("4.1.2.3.8");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("4.1.2.3.9");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("4.1.2.3", new System.Windows.Forms.TreeNode[] {
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("4.1.2.4.1");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("4.1.2.4", new System.Windows.Forms.TreeNode[] {
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("4.1.2.5.1");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("4.1.2.5.2");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("4.1.2.5.3");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("4.1.2.5", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20,
            treeNode21});
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("4.1.3.1.1");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("4.1.3.1.2");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("4.1.3.1", new System.Windows.Forms.TreeNode[] {
            treeNode23,
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("4.1.3.2.1");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("4.1.3.2.2");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("4.1.3.2", new System.Windows.Forms.TreeNode[] {
            treeNode26,
            treeNode27});
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("4.1.4.1.1");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("4.1.4.1.2");
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("4.1.4.1", new System.Windows.Forms.TreeNode[] {
            treeNode29,
            treeNode30});
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("4.1.4.2.1");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("4.1.4.2.2");
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("4.1.4.2.3");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("4.1.4.2.4");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("4.1.4.2", new System.Windows.Forms.TreeNode[] {
            treeNode32,
            treeNode33,
            treeNode34,
            treeNode35});
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("4.1.5.1.1");
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("4.1.5.1.2");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("4.1.5.1", new System.Windows.Forms.TreeNode[] {
            treeNode37,
            treeNode38});
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("4.1.5.3.1");
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("4.1.5.3", new System.Windows.Forms.TreeNode[] {
            treeNode40});
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("4.1.5.4.1");
            System.Windows.Forms.TreeNode treeNode43 = new System.Windows.Forms.TreeNode("4.1.5.4.2");
            System.Windows.Forms.TreeNode treeNode44 = new System.Windows.Forms.TreeNode("4.1.5.4", new System.Windows.Forms.TreeNode[] {
            treeNode42,
            treeNode43});
            System.Windows.Forms.TreeNode treeNode45 = new System.Windows.Forms.TreeNode("4.2.2.1.2");
            System.Windows.Forms.TreeNode treeNode46 = new System.Windows.Forms.TreeNode("4.2.2.1", new System.Windows.Forms.TreeNode[] {
            treeNode45});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tb_finalText = new System.Windows.Forms.RichTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gb_subRule1 = new System.Windows.Forms.GroupBox();
            this.rb_rule1Fail = new System.Windows.Forms.RadioButton();
            this.rb_rule1Accept = new System.Windows.Forms.RadioButton();
            this.detail1 = new System.Windows.Forms.TextBox();
            this.btn_Picture1 = new System.Windows.Forms.Button();
            this.gb_subRule2 = new System.Windows.Forms.GroupBox();
            this.rb_rule2Fail = new System.Windows.Forms.RadioButton();
            this.rb_rule2Accept = new System.Windows.Forms.RadioButton();
            this.detail2 = new System.Windows.Forms.TextBox();
            this.btn_Picture2 = new System.Windows.Forms.Button();
            this.gb_subRule7 = new System.Windows.Forms.GroupBox();
            this.btn_Picture7 = new System.Windows.Forms.Button();
            this.rb_rule7Fail = new System.Windows.Forms.RadioButton();
            this.rb_rule7Accept = new System.Windows.Forms.RadioButton();
            this.detail7 = new System.Windows.Forms.TextBox();
            this.gb_subRule3 = new System.Windows.Forms.GroupBox();
            this.rb_rule3Fail = new System.Windows.Forms.RadioButton();
            this.rb_rule3Accept = new System.Windows.Forms.RadioButton();
            this.detail3 = new System.Windows.Forms.TextBox();
            this.btn_Picture3 = new System.Windows.Forms.Button();
            this.gb_subRule6 = new System.Windows.Forms.GroupBox();
            this.rb_rule6Fail = new System.Windows.Forms.RadioButton();
            this.btn_Picture6 = new System.Windows.Forms.Button();
            this.rb_rule6Accept = new System.Windows.Forms.RadioButton();
            this.detail6 = new System.Windows.Forms.TextBox();
            this.gb_subRule4 = new System.Windows.Forms.GroupBox();
            this.rb_rule4Fail = new System.Windows.Forms.RadioButton();
            this.rb_rule4Accept = new System.Windows.Forms.RadioButton();
            this.detail4 = new System.Windows.Forms.TextBox();
            this.btn_Picture4 = new System.Windows.Forms.Button();
            this.gb_subRule5 = new System.Windows.Forms.GroupBox();
            this.rb_rule5Fail = new System.Windows.Forms.RadioButton();
            this.btn_Picture5 = new System.Windows.Forms.Button();
            this.rb_rule5Accept = new System.Windows.Forms.RadioButton();
            this.detail5 = new System.Windows.Forms.TextBox();
            this.functions = new System.Windows.Forms.GroupBox();
            this.btn_PassCondition = new System.Windows.Forms.Button();
            this.btn_newJsonr = new System.Windows.Forms.Button();
            this.btn_saveNewJsonr = new System.Windows.Forms.Button();
            this.label_finalResult = new System.Windows.Forms.Label();
            this.btn_loadJsonr = new System.Windows.Forms.Button();
            this.btn_notFit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_fail = new System.Windows.Forms.Button();
            this.btn_saveJsonr = new System.Windows.Forms.Button();
            this.btn_accept = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tb_outputDetail = new System.Windows.Forms.TextBox();
            this.btn_outputDocx = new System.Windows.Forms.Button();
            this.btn_readApplication = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tb_reportVersion = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dtp_reportDate = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.dtp_finishDate = new System.Windows.Forms.DateTimePicker();
            this.dtp_startDate = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_reportNo = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_companyName = new System.Windows.Forms.TextBox();
            this.tb_companyAddr = new System.Windows.Forms.TextBox();
            this.tb_developerName = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox_TestingClass = new System.Windows.Forms.GroupBox();
            this.rb_class3 = new System.Windows.Forms.RadioButton();
            this.rb_class1 = new System.Windows.Forms.RadioButton();
            this.rb_class2 = new System.Windows.Forms.RadioButton();
            this.platform = new System.Windows.Forms.GroupBox();
            this.rb_android = new System.Windows.Forms.RadioButton();
            this.rb_iOS = new System.Windows.Forms.RadioButton();
            this.tb_appVersion = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_appSHA1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_appId = new System.Windows.Forms.TextBox();
            this.tb_appCommonName = new System.Windows.Forms.TextBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.openJsonrDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveJsonrDialog = new System.Windows.Forms.SaveFileDialog();
            this.openApplicationDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveDocxDialog = new System.Windows.Forms.SaveFileDialog();
            this.btn_toolBox = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gb_subRule1.SuspendLayout();
            this.gb_subRule2.SuspendLayout();
            this.gb_subRule7.SuspendLayout();
            this.gb_subRule3.SuspendLayout();
            this.gb_subRule6.SuspendLayout();
            this.gb_subRule4.SuspendLayout();
            this.gb_subRule5.SuspendLayout();
            this.functions.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox_TestingClass.SuspendLayout();
            this.platform.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(907, 537);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tb_finalText);
            this.tabPage1.Controls.Add(this.comboBox1);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.functions);
            this.tabPage1.Controls.Add(this.treeView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(899, 511);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "測項";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tb_finalText
            // 
            this.tb_finalText.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_finalText.Location = new System.Drawing.Point(466, 3);
            this.tb_finalText.Name = "tb_finalText";
            this.tb_finalText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.tb_finalText.Size = new System.Drawing.Size(430, 505);
            this.tb_finalText.TabIndex = 15;
            this.tb_finalText.Text = "";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "甲級",
            "乙級",
            "丙級"});
            this.comboBox1.Location = new System.Drawing.Point(3, 6);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(106, 20);
            this.comboBox1.TabIndex = 13;
            this.comboBox1.Text = "請選擇檢測分級";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.gb_subRule1);
            this.panel1.Controls.Add(this.gb_subRule2);
            this.panel1.Controls.Add(this.gb_subRule7);
            this.panel1.Controls.Add(this.gb_subRule3);
            this.panel1.Controls.Add(this.gb_subRule6);
            this.panel1.Controls.Add(this.gb_subRule4);
            this.panel1.Controls.Add(this.gb_subRule5);
            this.panel1.Location = new System.Drawing.Point(115, 119);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(345, 386);
            this.panel1.TabIndex = 14;
            // 
            // gb_subRule1
            // 
            this.gb_subRule1.Controls.Add(this.rb_rule1Fail);
            this.gb_subRule1.Controls.Add(this.rb_rule1Accept);
            this.gb_subRule1.Controls.Add(this.detail1);
            this.gb_subRule1.Controls.Add(this.btn_Picture1);
            this.gb_subRule1.Location = new System.Drawing.Point(3, 3);
            this.gb_subRule1.Name = "gb_subRule1";
            this.gb_subRule1.Size = new System.Drawing.Size(323, 111);
            this.gb_subRule1.TabIndex = 7;
            this.gb_subRule1.TabStop = false;
            this.gb_subRule1.Text = "基準(1)";
            // 
            // rb_rule1Fail
            // 
            this.rb_rule1Fail.AutoSize = true;
            this.rb_rule1Fail.Enabled = false;
            this.rb_rule1Fail.Location = new System.Drawing.Point(264, 43);
            this.rb_rule1Fail.Name = "rb_rule1Fail";
            this.rb_rule1Fail.Size = new System.Drawing.Size(59, 16);
            this.rb_rule1Fail.TabIndex = 7;
            this.rb_rule1Fail.TabStop = true;
            this.rb_rule1Fail.Text = "不符合";
            this.rb_rule1Fail.UseVisualStyleBackColor = true;
            // 
            // rb_rule1Accept
            // 
            this.rb_rule1Accept.AutoSize = true;
            this.rb_rule1Accept.Enabled = false;
            this.rb_rule1Accept.Location = new System.Drawing.Point(264, 21);
            this.rb_rule1Accept.Name = "rb_rule1Accept";
            this.rb_rule1Accept.Size = new System.Drawing.Size(47, 16);
            this.rb_rule1Accept.TabIndex = 6;
            this.rb_rule1Accept.TabStop = true;
            this.rb_rule1Accept.Text = "符合";
            this.rb_rule1Accept.UseVisualStyleBackColor = true;
            // 
            // detail1
            // 
            this.detail1.Font = new System.Drawing.Font("新細明體", 10F);
            this.detail1.Location = new System.Drawing.Point(6, 21);
            this.detail1.Multiline = true;
            this.detail1.Name = "detail1";
            this.detail1.ReadOnly = true;
            this.detail1.Size = new System.Drawing.Size(252, 84);
            this.detail1.TabIndex = 5;
            // 
            // btn_Picture1
            // 
            this.btn_Picture1.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Picture1.FlatAppearance.BorderSize = 2;
            this.btn_Picture1.Location = new System.Drawing.Point(264, 65);
            this.btn_Picture1.Name = "btn_Picture1";
            this.btn_Picture1.Size = new System.Drawing.Size(53, 26);
            this.btn_Picture1.TabIndex = 14;
            this.btn_Picture1.Tag = "0";
            this.btn_Picture1.Text = "圖片";
            this.btn_Picture1.UseVisualStyleBackColor = true;
            // 
            // gb_subRule2
            // 
            this.gb_subRule2.Controls.Add(this.rb_rule2Fail);
            this.gb_subRule2.Controls.Add(this.rb_rule2Accept);
            this.gb_subRule2.Controls.Add(this.detail2);
            this.gb_subRule2.Controls.Add(this.btn_Picture2);
            this.gb_subRule2.Location = new System.Drawing.Point(3, 120);
            this.gb_subRule2.Name = "gb_subRule2";
            this.gb_subRule2.Size = new System.Drawing.Size(323, 111);
            this.gb_subRule2.TabIndex = 8;
            this.gb_subRule2.TabStop = false;
            this.gb_subRule2.Text = "基準(2)";
            // 
            // rb_rule2Fail
            // 
            this.rb_rule2Fail.AutoSize = true;
            this.rb_rule2Fail.Enabled = false;
            this.rb_rule2Fail.Location = new System.Drawing.Point(264, 41);
            this.rb_rule2Fail.Name = "rb_rule2Fail";
            this.rb_rule2Fail.Size = new System.Drawing.Size(59, 16);
            this.rb_rule2Fail.TabIndex = 7;
            this.rb_rule2Fail.TabStop = true;
            this.rb_rule2Fail.Text = "不符合";
            this.rb_rule2Fail.UseVisualStyleBackColor = true;
            // 
            // rb_rule2Accept
            // 
            this.rb_rule2Accept.AutoSize = true;
            this.rb_rule2Accept.Enabled = false;
            this.rb_rule2Accept.Location = new System.Drawing.Point(264, 21);
            this.rb_rule2Accept.Name = "rb_rule2Accept";
            this.rb_rule2Accept.Size = new System.Drawing.Size(47, 16);
            this.rb_rule2Accept.TabIndex = 6;
            this.rb_rule2Accept.TabStop = true;
            this.rb_rule2Accept.Text = "符合";
            this.rb_rule2Accept.UseVisualStyleBackColor = true;
            // 
            // detail2
            // 
            this.detail2.Font = new System.Drawing.Font("新細明體", 10F);
            this.detail2.Location = new System.Drawing.Point(6, 21);
            this.detail2.Multiline = true;
            this.detail2.Name = "detail2";
            this.detail2.ReadOnly = true;
            this.detail2.Size = new System.Drawing.Size(252, 84);
            this.detail2.TabIndex = 5;
            // 
            // btn_Picture2
            // 
            this.btn_Picture2.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Picture2.FlatAppearance.BorderSize = 2;
            this.btn_Picture2.Location = new System.Drawing.Point(264, 63);
            this.btn_Picture2.Name = "btn_Picture2";
            this.btn_Picture2.Size = new System.Drawing.Size(53, 26);
            this.btn_Picture2.TabIndex = 15;
            this.btn_Picture2.Tag = "1";
            this.btn_Picture2.Text = "圖片";
            this.btn_Picture2.UseVisualStyleBackColor = true;
            // 
            // gb_subRule7
            // 
            this.gb_subRule7.Controls.Add(this.btn_Picture7);
            this.gb_subRule7.Controls.Add(this.rb_rule7Fail);
            this.gb_subRule7.Controls.Add(this.rb_rule7Accept);
            this.gb_subRule7.Controls.Add(this.detail7);
            this.gb_subRule7.Location = new System.Drawing.Point(3, 705);
            this.gb_subRule7.Name = "gb_subRule7";
            this.gb_subRule7.Size = new System.Drawing.Size(323, 111);
            this.gb_subRule7.TabIndex = 13;
            this.gb_subRule7.TabStop = false;
            this.gb_subRule7.Text = "基準(7)";
            this.gb_subRule7.Visible = false;
            // 
            // btn_Picture7
            // 
            this.btn_Picture7.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Picture7.FlatAppearance.BorderSize = 2;
            this.btn_Picture7.Location = new System.Drawing.Point(264, 63);
            this.btn_Picture7.Name = "btn_Picture7";
            this.btn_Picture7.Size = new System.Drawing.Size(53, 26);
            this.btn_Picture7.TabIndex = 20;
            this.btn_Picture7.Tag = "6";
            this.btn_Picture7.Text = "圖片";
            this.btn_Picture7.UseVisualStyleBackColor = true;
            // 
            // rb_rule7Fail
            // 
            this.rb_rule7Fail.AutoSize = true;
            this.rb_rule7Fail.Enabled = false;
            this.rb_rule7Fail.Location = new System.Drawing.Point(264, 41);
            this.rb_rule7Fail.Name = "rb_rule7Fail";
            this.rb_rule7Fail.Size = new System.Drawing.Size(59, 16);
            this.rb_rule7Fail.TabIndex = 7;
            this.rb_rule7Fail.TabStop = true;
            this.rb_rule7Fail.Text = "不符合";
            this.rb_rule7Fail.UseVisualStyleBackColor = true;
            // 
            // rb_rule7Accept
            // 
            this.rb_rule7Accept.AutoSize = true;
            this.rb_rule7Accept.Enabled = false;
            this.rb_rule7Accept.Location = new System.Drawing.Point(264, 19);
            this.rb_rule7Accept.Name = "rb_rule7Accept";
            this.rb_rule7Accept.Size = new System.Drawing.Size(47, 16);
            this.rb_rule7Accept.TabIndex = 6;
            this.rb_rule7Accept.TabStop = true;
            this.rb_rule7Accept.Text = "符合";
            this.rb_rule7Accept.UseVisualStyleBackColor = true;
            // 
            // detail7
            // 
            this.detail7.Font = new System.Drawing.Font("新細明體", 10F);
            this.detail7.Location = new System.Drawing.Point(6, 19);
            this.detail7.Multiline = true;
            this.detail7.Name = "detail7";
            this.detail7.ReadOnly = true;
            this.detail7.Size = new System.Drawing.Size(252, 84);
            this.detail7.TabIndex = 5;
            // 
            // gb_subRule3
            // 
            this.gb_subRule3.Controls.Add(this.rb_rule3Fail);
            this.gb_subRule3.Controls.Add(this.rb_rule3Accept);
            this.gb_subRule3.Controls.Add(this.detail3);
            this.gb_subRule3.Controls.Add(this.btn_Picture3);
            this.gb_subRule3.Location = new System.Drawing.Point(3, 237);
            this.gb_subRule3.Name = "gb_subRule3";
            this.gb_subRule3.Size = new System.Drawing.Size(323, 111);
            this.gb_subRule3.TabIndex = 9;
            this.gb_subRule3.TabStop = false;
            this.gb_subRule3.Text = "基準(3)";
            // 
            // rb_rule3Fail
            // 
            this.rb_rule3Fail.AutoSize = true;
            this.rb_rule3Fail.Enabled = false;
            this.rb_rule3Fail.Location = new System.Drawing.Point(264, 41);
            this.rb_rule3Fail.Name = "rb_rule3Fail";
            this.rb_rule3Fail.Size = new System.Drawing.Size(59, 16);
            this.rb_rule3Fail.TabIndex = 7;
            this.rb_rule3Fail.TabStop = true;
            this.rb_rule3Fail.Text = "不符合";
            this.rb_rule3Fail.UseVisualStyleBackColor = true;
            // 
            // rb_rule3Accept
            // 
            this.rb_rule3Accept.AutoSize = true;
            this.rb_rule3Accept.Enabled = false;
            this.rb_rule3Accept.Location = new System.Drawing.Point(264, 19);
            this.rb_rule3Accept.Name = "rb_rule3Accept";
            this.rb_rule3Accept.Size = new System.Drawing.Size(47, 16);
            this.rb_rule3Accept.TabIndex = 6;
            this.rb_rule3Accept.TabStop = true;
            this.rb_rule3Accept.Text = "符合";
            this.rb_rule3Accept.UseVisualStyleBackColor = true;
            // 
            // detail3
            // 
            this.detail3.Font = new System.Drawing.Font("新細明體", 10F);
            this.detail3.Location = new System.Drawing.Point(6, 19);
            this.detail3.Multiline = true;
            this.detail3.Name = "detail3";
            this.detail3.ReadOnly = true;
            this.detail3.Size = new System.Drawing.Size(252, 84);
            this.detail3.TabIndex = 5;
            // 
            // btn_Picture3
            // 
            this.btn_Picture3.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Picture3.FlatAppearance.BorderSize = 2;
            this.btn_Picture3.Location = new System.Drawing.Point(264, 63);
            this.btn_Picture3.Name = "btn_Picture3";
            this.btn_Picture3.Size = new System.Drawing.Size(53, 26);
            this.btn_Picture3.TabIndex = 16;
            this.btn_Picture3.Tag = "2";
            this.btn_Picture3.Text = "圖片";
            this.btn_Picture3.UseVisualStyleBackColor = true;
            // 
            // gb_subRule6
            // 
            this.gb_subRule6.Controls.Add(this.rb_rule6Fail);
            this.gb_subRule6.Controls.Add(this.btn_Picture6);
            this.gb_subRule6.Controls.Add(this.rb_rule6Accept);
            this.gb_subRule6.Controls.Add(this.detail6);
            this.gb_subRule6.Location = new System.Drawing.Point(3, 588);
            this.gb_subRule6.Name = "gb_subRule6";
            this.gb_subRule6.Size = new System.Drawing.Size(323, 111);
            this.gb_subRule6.TabIndex = 12;
            this.gb_subRule6.TabStop = false;
            this.gb_subRule6.Text = "基準(6)";
            this.gb_subRule6.Visible = false;
            // 
            // rb_rule6Fail
            // 
            this.rb_rule6Fail.AutoSize = true;
            this.rb_rule6Fail.Enabled = false;
            this.rb_rule6Fail.Location = new System.Drawing.Point(264, 41);
            this.rb_rule6Fail.Name = "rb_rule6Fail";
            this.rb_rule6Fail.Size = new System.Drawing.Size(59, 16);
            this.rb_rule6Fail.TabIndex = 7;
            this.rb_rule6Fail.TabStop = true;
            this.rb_rule6Fail.Text = "不符合";
            this.rb_rule6Fail.UseVisualStyleBackColor = true;
            // 
            // btn_Picture6
            // 
            this.btn_Picture6.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Picture6.FlatAppearance.BorderSize = 2;
            this.btn_Picture6.Location = new System.Drawing.Point(264, 63);
            this.btn_Picture6.Name = "btn_Picture6";
            this.btn_Picture6.Size = new System.Drawing.Size(53, 26);
            this.btn_Picture6.TabIndex = 19;
            this.btn_Picture6.Tag = "5";
            this.btn_Picture6.Text = "圖片";
            this.btn_Picture6.UseVisualStyleBackColor = true;
            // 
            // rb_rule6Accept
            // 
            this.rb_rule6Accept.AutoSize = true;
            this.rb_rule6Accept.Enabled = false;
            this.rb_rule6Accept.Location = new System.Drawing.Point(264, 19);
            this.rb_rule6Accept.Name = "rb_rule6Accept";
            this.rb_rule6Accept.Size = new System.Drawing.Size(47, 16);
            this.rb_rule6Accept.TabIndex = 6;
            this.rb_rule6Accept.TabStop = true;
            this.rb_rule6Accept.Text = "符合";
            this.rb_rule6Accept.UseVisualStyleBackColor = true;
            // 
            // detail6
            // 
            this.detail6.Font = new System.Drawing.Font("新細明體", 10F);
            this.detail6.Location = new System.Drawing.Point(6, 19);
            this.detail6.Multiline = true;
            this.detail6.Name = "detail6";
            this.detail6.ReadOnly = true;
            this.detail6.Size = new System.Drawing.Size(252, 84);
            this.detail6.TabIndex = 5;
            // 
            // gb_subRule4
            // 
            this.gb_subRule4.Controls.Add(this.rb_rule4Fail);
            this.gb_subRule4.Controls.Add(this.rb_rule4Accept);
            this.gb_subRule4.Controls.Add(this.detail4);
            this.gb_subRule4.Controls.Add(this.btn_Picture4);
            this.gb_subRule4.Location = new System.Drawing.Point(3, 354);
            this.gb_subRule4.Name = "gb_subRule4";
            this.gb_subRule4.Size = new System.Drawing.Size(323, 111);
            this.gb_subRule4.TabIndex = 10;
            this.gb_subRule4.TabStop = false;
            this.gb_subRule4.Text = "基準(4)";
            this.gb_subRule4.Visible = false;
            // 
            // rb_rule4Fail
            // 
            this.rb_rule4Fail.AutoSize = true;
            this.rb_rule4Fail.Enabled = false;
            this.rb_rule4Fail.Location = new System.Drawing.Point(264, 41);
            this.rb_rule4Fail.Name = "rb_rule4Fail";
            this.rb_rule4Fail.Size = new System.Drawing.Size(59, 16);
            this.rb_rule4Fail.TabIndex = 7;
            this.rb_rule4Fail.TabStop = true;
            this.rb_rule4Fail.Text = "不符合";
            this.rb_rule4Fail.UseVisualStyleBackColor = true;
            // 
            // rb_rule4Accept
            // 
            this.rb_rule4Accept.AutoSize = true;
            this.rb_rule4Accept.Enabled = false;
            this.rb_rule4Accept.Location = new System.Drawing.Point(264, 19);
            this.rb_rule4Accept.Name = "rb_rule4Accept";
            this.rb_rule4Accept.Size = new System.Drawing.Size(47, 16);
            this.rb_rule4Accept.TabIndex = 6;
            this.rb_rule4Accept.TabStop = true;
            this.rb_rule4Accept.Text = "符合";
            this.rb_rule4Accept.UseVisualStyleBackColor = true;
            // 
            // detail4
            // 
            this.detail4.Font = new System.Drawing.Font("新細明體", 10F);
            this.detail4.Location = new System.Drawing.Point(6, 19);
            this.detail4.Multiline = true;
            this.detail4.Name = "detail4";
            this.detail4.ReadOnly = true;
            this.detail4.Size = new System.Drawing.Size(252, 84);
            this.detail4.TabIndex = 5;
            // 
            // btn_Picture4
            // 
            this.btn_Picture4.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Picture4.FlatAppearance.BorderSize = 2;
            this.btn_Picture4.Location = new System.Drawing.Point(264, 63);
            this.btn_Picture4.Name = "btn_Picture4";
            this.btn_Picture4.Size = new System.Drawing.Size(53, 26);
            this.btn_Picture4.TabIndex = 17;
            this.btn_Picture4.Tag = "3";
            this.btn_Picture4.Text = "圖片";
            this.btn_Picture4.UseVisualStyleBackColor = true;
            // 
            // gb_subRule5
            // 
            this.gb_subRule5.Controls.Add(this.rb_rule5Fail);
            this.gb_subRule5.Controls.Add(this.btn_Picture5);
            this.gb_subRule5.Controls.Add(this.rb_rule5Accept);
            this.gb_subRule5.Controls.Add(this.detail5);
            this.gb_subRule5.Location = new System.Drawing.Point(3, 471);
            this.gb_subRule5.Name = "gb_subRule5";
            this.gb_subRule5.Size = new System.Drawing.Size(323, 111);
            this.gb_subRule5.TabIndex = 11;
            this.gb_subRule5.TabStop = false;
            this.gb_subRule5.Text = "基準(5)";
            this.gb_subRule5.Visible = false;
            // 
            // rb_rule5Fail
            // 
            this.rb_rule5Fail.AutoSize = true;
            this.rb_rule5Fail.Enabled = false;
            this.rb_rule5Fail.Location = new System.Drawing.Point(264, 41);
            this.rb_rule5Fail.Name = "rb_rule5Fail";
            this.rb_rule5Fail.Size = new System.Drawing.Size(59, 16);
            this.rb_rule5Fail.TabIndex = 7;
            this.rb_rule5Fail.TabStop = true;
            this.rb_rule5Fail.Text = "不符合";
            this.rb_rule5Fail.UseVisualStyleBackColor = true;
            // 
            // btn_Picture5
            // 
            this.btn_Picture5.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_Picture5.FlatAppearance.BorderSize = 2;
            this.btn_Picture5.Location = new System.Drawing.Point(264, 63);
            this.btn_Picture5.Name = "btn_Picture5";
            this.btn_Picture5.Size = new System.Drawing.Size(53, 26);
            this.btn_Picture5.TabIndex = 18;
            this.btn_Picture5.Tag = "4";
            this.btn_Picture5.Text = "圖片";
            this.btn_Picture5.UseVisualStyleBackColor = true;
            // 
            // rb_rule5Accept
            // 
            this.rb_rule5Accept.AutoSize = true;
            this.rb_rule5Accept.Enabled = false;
            this.rb_rule5Accept.Location = new System.Drawing.Point(264, 19);
            this.rb_rule5Accept.Name = "rb_rule5Accept";
            this.rb_rule5Accept.Size = new System.Drawing.Size(47, 16);
            this.rb_rule5Accept.TabIndex = 6;
            this.rb_rule5Accept.TabStop = true;
            this.rb_rule5Accept.Text = "符合";
            this.rb_rule5Accept.UseVisualStyleBackColor = true;
            // 
            // detail5
            // 
            this.detail5.Font = new System.Drawing.Font("新細明體", 10F);
            this.detail5.Location = new System.Drawing.Point(6, 19);
            this.detail5.Multiline = true;
            this.detail5.Name = "detail5";
            this.detail5.ReadOnly = true;
            this.detail5.Size = new System.Drawing.Size(252, 84);
            this.detail5.TabIndex = 5;
            // 
            // functions
            // 
            this.functions.Controls.Add(this.btn_toolBox);
            this.functions.Controls.Add(this.btn_PassCondition);
            this.functions.Controls.Add(this.btn_newJsonr);
            this.functions.Controls.Add(this.btn_saveNewJsonr);
            this.functions.Controls.Add(this.label_finalResult);
            this.functions.Controls.Add(this.btn_loadJsonr);
            this.functions.Controls.Add(this.btn_notFit);
            this.functions.Controls.Add(this.label1);
            this.functions.Controls.Add(this.btn_fail);
            this.functions.Controls.Add(this.btn_saveJsonr);
            this.functions.Controls.Add(this.btn_accept);
            this.functions.Location = new System.Drawing.Point(115, 3);
            this.functions.Name = "functions";
            this.functions.Size = new System.Drawing.Size(345, 105);
            this.functions.TabIndex = 8;
            this.functions.TabStop = false;
            this.functions.Text = "功能";
            // 
            // btn_PassCondition
            // 
            this.btn_PassCondition.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_PassCondition.FlatAppearance.BorderSize = 2;
            this.btn_PassCondition.Location = new System.Drawing.Point(258, 44);
            this.btn_PassCondition.Name = "btn_PassCondition";
            this.btn_PassCondition.Size = new System.Drawing.Size(77, 26);
            this.btn_PassCondition.TabIndex = 13;
            this.btn_PassCondition.Text = "判定規則";
            this.btn_PassCondition.UseVisualStyleBackColor = true;
            this.btn_PassCondition.Click += new System.EventHandler(this.Btn_PassCondition_Click);
            // 
            // btn_newJsonr
            // 
            this.btn_newJsonr.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.btn_newJsonr.FlatAppearance.BorderSize = 2;
            this.btn_newJsonr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_newJsonr.Location = new System.Drawing.Point(9, 12);
            this.btn_newJsonr.Name = "btn_newJsonr";
            this.btn_newJsonr.Size = new System.Drawing.Size(77, 26);
            this.btn_newJsonr.TabIndex = 12;
            this.btn_newJsonr.Text = "開新進度";
            this.btn_newJsonr.UseVisualStyleBackColor = true;
            this.btn_newJsonr.Click += new System.EventHandler(this.Btn_newJsonr_Click);
            // 
            // btn_saveNewJsonr
            // 
            this.btn_saveNewJsonr.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btn_saveNewJsonr.FlatAppearance.BorderSize = 2;
            this.btn_saveNewJsonr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_saveNewJsonr.Location = new System.Drawing.Point(258, 12);
            this.btn_saveNewJsonr.Name = "btn_saveNewJsonr";
            this.btn_saveNewJsonr.Size = new System.Drawing.Size(77, 26);
            this.btn_saveNewJsonr.TabIndex = 11;
            this.btn_saveNewJsonr.Text = "另存新進度";
            this.btn_saveNewJsonr.UseVisualStyleBackColor = true;
            this.btn_saveNewJsonr.Click += new System.EventHandler(this.Btn_saveNewJsonr_Click);
            // 
            // label_finalResult
            // 
            this.label_finalResult.AutoSize = true;
            this.label_finalResult.Font = new System.Drawing.Font("新細明體", 12F);
            this.label_finalResult.Location = new System.Drawing.Point(97, 82);
            this.label_finalResult.Name = "label_finalResult";
            this.label_finalResult.Size = new System.Drawing.Size(72, 16);
            this.label_finalResult.TabIndex = 4;
            this.label_finalResult.Text = "尚未檢測";
            // 
            // btn_loadJsonr
            // 
            this.btn_loadJsonr.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.btn_loadJsonr.FlatAppearance.BorderSize = 2;
            this.btn_loadJsonr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_loadJsonr.Location = new System.Drawing.Point(92, 12);
            this.btn_loadJsonr.Name = "btn_loadJsonr";
            this.btn_loadJsonr.Size = new System.Drawing.Size(77, 26);
            this.btn_loadJsonr.TabIndex = 9;
            this.btn_loadJsonr.Text = "載入進度";
            this.btn_loadJsonr.UseVisualStyleBackColor = true;
            this.btn_loadJsonr.Click += new System.EventHandler(this.Btn_loadJsonr_Click);
            // 
            // btn_notFit
            // 
            this.btn_notFit.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_notFit.FlatAppearance.BorderSize = 2;
            this.btn_notFit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_notFit.Location = new System.Drawing.Point(175, 44);
            this.btn_notFit.Name = "btn_notFit";
            this.btn_notFit.Size = new System.Drawing.Size(77, 26);
            this.btn_notFit.TabIndex = 2;
            this.btn_notFit.Text = "不適用";
            this.btn_notFit.UseVisualStyleBackColor = true;
            this.btn_notFit.Click += new System.EventHandler(this.Btn_notFit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F);
            this.label1.Location = new System.Drawing.Point(6, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "檢測結果：";
            // 
            // btn_fail
            // 
            this.btn_fail.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btn_fail.FlatAppearance.BorderSize = 2;
            this.btn_fail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_fail.Location = new System.Drawing.Point(92, 44);
            this.btn_fail.Name = "btn_fail";
            this.btn_fail.Size = new System.Drawing.Size(77, 26);
            this.btn_fail.TabIndex = 1;
            this.btn_fail.Text = "不符合";
            this.btn_fail.UseVisualStyleBackColor = true;
            this.btn_fail.Click += new System.EventHandler(this.Btn_fail_Click);
            // 
            // btn_saveJsonr
            // 
            this.btn_saveJsonr.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.btn_saveJsonr.FlatAppearance.BorderSize = 2;
            this.btn_saveJsonr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_saveJsonr.Location = new System.Drawing.Point(175, 12);
            this.btn_saveJsonr.Name = "btn_saveJsonr";
            this.btn_saveJsonr.Size = new System.Drawing.Size(77, 26);
            this.btn_saveJsonr.TabIndex = 8;
            this.btn_saveJsonr.Text = "儲存進度";
            this.btn_saveJsonr.UseVisualStyleBackColor = true;
            this.btn_saveJsonr.Click += new System.EventHandler(this.Btn_saveJsonr_Click);
            // 
            // btn_accept
            // 
            this.btn_accept.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.btn_accept.FlatAppearance.BorderSize = 2;
            this.btn_accept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_accept.Location = new System.Drawing.Point(9, 44);
            this.btn_accept.Name = "btn_accept";
            this.btn_accept.Size = new System.Drawing.Size(77, 26);
            this.btn_accept.TabIndex = 0;
            this.btn_accept.Text = "符合";
            this.btn_accept.UseVisualStyleBackColor = true;
            this.btn_accept.Click += new System.EventHandler(this.Btn_accept_Click);
            // 
            // treeView1
            // 
            this.treeView1.Font = new System.Drawing.Font("新細明體", 11F);
            this.treeView1.Indent = 10;
            this.treeView1.ItemHeight = 20;
            this.treeView1.Location = new System.Drawing.Point(3, 29);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node41112";
            treeNode1.Text = "4.1.1.1.2";
            treeNode1.ToolTipText = "行動應用程式應於發布時說明欲存取之安全敏感性資料、行動裝置資源及宣告之權限用途";
            treeNode2.Checked = true;
            treeNode2.Name = "Node4111";
            treeNode2.Text = "4.1.1.1";
            treeNode2.ToolTipText = "行動應用程式發布";
            treeNode3.Name = "Node41131";
            treeNode3.Text = "4.1.1.3.1";
            treeNode3.ToolTipText = "行動應用程式開發者應提供回報安全性問題之管道";
            treeNode4.Name = "Node4113";
            treeNode4.Text = "4.1.1.3";
            treeNode4.ToolTipText = "行動應用程式安全性問題回報";
            treeNode5.Name = "Node41211";
            treeNode5.Text = "4.1.2.1.1";
            treeNode5.ToolTipText = "行動應用程式應於蒐集安全敏感性資料前，取得使用者同意";
            treeNode6.Name = "Node41212";
            treeNode6.Text = "4.1.2.1.2";
            treeNode6.ToolTipText = "行動應用程式應提供使用者拒絕蒐集安全敏感性資料之權利";
            treeNode7.Name = "Node4121";
            treeNode7.Text = "4.1.2.1";
            treeNode7.ToolTipText = "敏感性資料蒐集";
            treeNode8.Name = "Node41231";
            treeNode8.Text = "4.1.2.3.1";
            treeNode8.ToolTipText = "行動應用程式應於儲存安全敏感性資料前，取得使用者同意";
            treeNode9.Name = "Node41232";
            treeNode9.Text = "4.1.2.3.2";
            treeNode9.ToolTipText = "行動應用程式應提供使用者拒絕儲存安全敏感性資料之權利";
            treeNode10.Name = "Node41234";
            treeNode10.Text = "4.1.2.3.4";
            treeNode10.ToolTipText = "行動應用程式應避免在關閉及登出後將安全敏感性資料儲存於冗餘檔案或日誌檔案中";
            treeNode11.Name = "Node41235";
            treeNode11.Text = "4.1.2.3.5";
            treeNode11.ToolTipText = "行動應用程式應避免將安全敏感性資料儲存於冗餘檔案或日誌檔案中";
            treeNode12.Name = "Node41236";
            treeNode12.Text = "4.1.2.3.6";
            treeNode12.ToolTipText = "安全敏感性資料應採用適當且有效之金鑰長度與加密演算法，進行加密處理再儲存";
            treeNode13.Name = "Node41237";
            treeNode13.Text = "4.1.2.3.7";
            treeNode13.ToolTipText = "安全敏感性資料應儲存於受作業系統保護之區域，以防止其他應用程式未經授權之存取";
            treeNode14.Name = "Node41238";
            treeNode14.Text = "4.1.2.3.8";
            treeNode14.ToolTipText = "安全敏感性資料應避免出現於行動應用程式之程式碼";
            treeNode15.Name = "Node41239";
            treeNode15.Text = "4.1.2.3.9";
            treeNode15.ToolTipText = "行動應用程式於畫面擷取時應主動警示使用者";
            treeNode16.Name = "Node4123";
            treeNode16.Text = "4.1.2.3";
            treeNode16.ToolTipText = "敏感性資料儲存";
            treeNode17.Name = "Node41241";
            treeNode17.Text = "4.1.2.4.1";
            treeNode17.ToolTipText = "行動應用程式透過網路傳輸安全敏感性資料，應使用適當且有效之金鑰長度與加密演算法進行安全加密";
            treeNode18.Name = "Node4124";
            treeNode18.Text = "4.1.2.4";
            treeNode18.ToolTipText = "敏感性資料傳輸";
            treeNode19.Name = "Node41251";
            treeNode19.Text = "4.1.2.5.1";
            treeNode19.ToolTipText = "行動裝置內之不同行動應用程式間，應於分享安全敏感性資料前，取得使用者同意";
            treeNode20.Name = "Node41252";
            treeNode20.Text = "4.1.2.5.2";
            treeNode20.ToolTipText = "行動應用程式應提供使用者拒絕分享安全敏感性資料之權利";
            treeNode21.Name = "Node41253";
            treeNode21.Text = "4.1.2.5.3";
            treeNode21.ToolTipText = "行動應用程式分享安全敏感性資料時，應避免未授權之行動應用程式存取";
            treeNode22.Name = "Node4125";
            treeNode22.Text = "4.1.2.5";
            treeNode22.ToolTipText = "敏感性資料分享";
            treeNode23.Name = "Node41311";
            treeNode23.Text = "4.1.3.1.1";
            treeNode23.ToolTipText = "行動應用程式應於使用交易資源前主動通知使用者";
            treeNode24.Name = "Node41312";
            treeNode24.Text = "4.1.3.1.2";
            treeNode24.ToolTipText = "行動應用程式應提供使用者拒絕使用交易資源之權利";
            treeNode25.Name = "Node4131";
            treeNode25.Text = "4.1.3.1";
            treeNode25.ToolTipText = "付費資源使用";
            treeNode26.Name = "Node41321";
            treeNode26.Text = "4.1.3.2.1";
            treeNode26.ToolTipText = "行動應用程式應於使用交易資源前進行使用者身分鑑別";
            treeNode27.Name = "Node41322";
            treeNode27.Text = "4.1.3.2.2";
            treeNode27.ToolTipText = "行動應用程式應記錄使用之交易資源與時間";
            treeNode28.Name = "Node4132";
            treeNode28.Text = "4.1.3.2";
            treeNode28.ToolTipText = "付費資源控管";
            treeNode29.Name = "Node41411";
            treeNode29.Text = "4.1.4.1.1";
            treeNode29.ToolTipText = "行動應用程式應有適當之身分鑑別機制，確認使用者身分";
            treeNode30.Name = "Node41412";
            treeNode30.Text = "4.1.4.1.2";
            treeNode30.ToolTipText = "行動應用程式應依使用者身分授權";
            treeNode31.Name = "Node4141";
            treeNode31.Text = "4.1.4.1";
            treeNode31.ToolTipText = "使用者身分認證與授權";
            treeNode32.Name = "Node41421";
            treeNode32.Text = "4.1.4.2.1";
            treeNode32.ToolTipText = "行動應用程式應避免使用具有規則性之交談識別碼";
            treeNode33.Name = "Node41422";
            treeNode33.Text = "4.1.4.2.2";
            treeNode33.ToolTipText = "行動應用程式應確認伺服器憑證之有效性";
            treeNode34.Name = "Node41423";
            treeNode34.Text = "4.1.4.2.3";
            treeNode34.ToolTipText = "行動應用程式應確認伺服器憑證為可信任之憑證機構所簽發";
            treeNode35.Name = "Node41424";
            treeNode35.Text = "4.1.4.2.4";
            treeNode35.ToolTipText = "行動應用程式應避免與未具有效憑證之伺服器，進行連線與傳輸安全敏感性資料";
            treeNode36.Name = "Node4142";
            treeNode36.Text = "4.1.4.2";
            treeNode36.ToolTipText = "連線管理機制";
            treeNode37.Name = "Node41511";
            treeNode37.Text = "4.1.5.1.1";
            treeNode37.ToolTipText = "行動應用程式應避免含有惡意程式碼";
            treeNode38.Name = "Node41512";
            treeNode38.Text = "4.1.5.1.2";
            treeNode38.ToolTipText = "行動應用程式應避免資訊安全漏洞";
            treeNode39.Name = "Node4151";
            treeNode39.Text = "4.1.5.1";
            treeNode39.ToolTipText = "防範惡意程式碼與避免資訊安全漏洞";
            treeNode40.Name = "Node41531";
            treeNode40.Text = "4.1.5.3.1";
            treeNode40.ToolTipText = "行動應用程式於引用之函式庫有更新時，應備妥對應之更新版本，更新方式請參酌 4.1.1.行動應用程式發布安全";
            treeNode41.Name = "Node4153";
            treeNode41.Text = "4.1.5.3";
            treeNode41.ToolTipText = "行動應用程式完整性";
            treeNode42.Name = "Node41541";
            treeNode42.Text = "4.1.5.4.1";
            treeNode42.ToolTipText = "行動應用程式應針對使用者於輸入階段之字串，進行安全檢查";
            treeNode43.Name = "Node41542";
            treeNode43.Text = "4.1.5.4.2";
            treeNode43.ToolTipText = "行動應用程式應提供相關注入攻擊防護機制";
            treeNode44.Name = "Node4154";
            treeNode44.Text = "4.1.5.4";
            treeNode44.ToolTipText = "使用者輸入驗證";
            treeNode45.Name = "Node42212";
            treeNode45.Text = "4.2.2.1.2";
            treeNode45.ToolTipText = "行動應用程式於 Webview 呈現功能時，所連線之網域應為安全網域";
            treeNode46.Name = "Node4221";
            treeNode46.Text = "4.2.2.1";
            treeNode46.ToolTipText = "Webview 安全檢測";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode4,
            treeNode7,
            treeNode16,
            treeNode18,
            treeNode22,
            treeNode25,
            treeNode28,
            treeNode31,
            treeNode36,
            treeNode39,
            treeNode41,
            treeNode44,
            treeNode46});
            this.treeView1.ShowNodeToolTips = true;
            this.treeView1.Size = new System.Drawing.Size(106, 476);
            this.treeView1.TabIndex = 12;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView1_AfterSelect);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tb_outputDetail);
            this.tabPage2.Controls.Add(this.btn_outputDocx);
            this.tabPage2.Controls.Add(this.btn_readApplication);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(899, 511);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "輸出報告";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tb_outputDetail
            // 
            this.tb_outputDetail.Font = new System.Drawing.Font("新細明體", 12F);
            this.tb_outputDetail.Location = new System.Drawing.Point(399, 219);
            this.tb_outputDetail.Multiline = true;
            this.tb_outputDetail.Name = "tb_outputDetail";
            this.tb_outputDetail.ReadOnly = true;
            this.tb_outputDetail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_outputDetail.Size = new System.Drawing.Size(488, 286);
            this.tb_outputDetail.TabIndex = 13;
            // 
            // btn_outputDocx
            // 
            this.btn_outputDocx.Location = new System.Drawing.Point(674, 18);
            this.btn_outputDocx.Name = "btn_outputDocx";
            this.btn_outputDocx.Size = new System.Drawing.Size(95, 41);
            this.btn_outputDocx.TabIndex = 12;
            this.btn_outputDocx.Text = "輸出成word檔";
            this.btn_outputDocx.UseVisualStyleBackColor = true;
            this.btn_outputDocx.Click += new System.EventHandler(this.Btn_outputDocx_Click);
            // 
            // btn_readApplication
            // 
            this.btn_readApplication.Location = new System.Drawing.Point(775, 18);
            this.btn_readApplication.Name = "btn_readApplication";
            this.btn_readApplication.Size = new System.Drawing.Size(95, 41);
            this.btn_readApplication.TabIndex = 14;
            this.btn_readApplication.Text = "從申請書載入資料";
            this.btn_readApplication.UseVisualStyleBackColor = true;
            this.btn_readApplication.Click += new System.EventHandler(this.Btn_readApplication_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tb_reportVersion);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.dtp_reportDate);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.dtp_finishDate);
            this.groupBox7.Controls.Add(this.dtp_startDate);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Location = new System.Drawing.Point(398, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(245, 207);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "日期";
            // 
            // tb_reportVersion
            // 
            this.tb_reportVersion.Location = new System.Drawing.Point(9, 169);
            this.tb_reportVersion.Name = "tb_reportVersion";
            this.tb_reportVersion.Size = new System.Drawing.Size(226, 22);
            this.tb_reportVersion.TabIndex = 12;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("新細明體", 12F);
            this.label14.Location = new System.Drawing.Point(6, 150);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 11;
            this.label14.Text = "報告版本：";
            // 
            // dtp_reportDate
            // 
            this.dtp_reportDate.Location = new System.Drawing.Point(9, 125);
            this.dtp_reportDate.Name = "dtp_reportDate";
            this.dtp_reportDate.Size = new System.Drawing.Size(226, 22);
            this.dtp_reportDate.TabIndex = 10;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("新細明體", 12F);
            this.label13.Location = new System.Drawing.Point(6, 106);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(88, 16);
            this.label13.TabIndex = 9;
            this.label13.Text = "報告日期：";
            // 
            // dtp_finishDate
            // 
            this.dtp_finishDate.Location = new System.Drawing.Point(9, 81);
            this.dtp_finishDate.Name = "dtp_finishDate";
            this.dtp_finishDate.Size = new System.Drawing.Size(226, 22);
            this.dtp_finishDate.TabIndex = 8;
            // 
            // dtp_startDate
            // 
            this.dtp_startDate.Location = new System.Drawing.Point(9, 37);
            this.dtp_startDate.Name = "dtp_startDate";
            this.dtp_startDate.Size = new System.Drawing.Size(226, 22);
            this.dtp_startDate.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("新細明體", 12F);
            this.label9.Location = new System.Drawing.Point(6, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(120, 16);
            this.label9.TabIndex = 6;
            this.label9.Text = "檢測起始日期：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("新細明體", 12F);
            this.label10.Location = new System.Drawing.Point(6, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 16);
            this.label10.TabIndex = 7;
            this.label10.Text = "檢測完成日期：";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.tb_reportNo);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(386, 499);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "基本資料";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("新細明體", 12F);
            this.label12.Location = new System.Drawing.Point(3, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 16);
            this.label12.TabIndex = 9;
            this.label12.Text = "報告編號：";
            // 
            // tb_reportNo
            // 
            this.tb_reportNo.Location = new System.Drawing.Point(6, 33);
            this.tb_reportNo.Name = "tb_reportNo";
            this.tb_reportNo.Size = new System.Drawing.Size(371, 22);
            this.tb_reportNo.TabIndex = 10;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label3);
            this.groupBox5.Controls.Add(this.label4);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.tb_companyName);
            this.groupBox5.Controls.Add(this.tb_companyAddr);
            this.groupBox5.Controls.Add(this.tb_developerName);
            this.groupBox5.Location = new System.Drawing.Point(6, 62);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(374, 154);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "送檢單位資訊";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F);
            this.label3.Location = new System.Drawing.Point(6, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "送檢單位名稱：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F);
            this.label4.Location = new System.Drawing.Point(6, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "送檢單位地址：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 12F);
            this.label5.Location = new System.Drawing.Point(6, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 16);
            this.label5.TabIndex = 2;
            this.label5.Text = "開發商名稱：";
            // 
            // tb_companyName
            // 
            this.tb_companyName.Location = new System.Drawing.Point(9, 37);
            this.tb_companyName.Name = "tb_companyName";
            this.tb_companyName.Size = new System.Drawing.Size(359, 22);
            this.tb_companyName.TabIndex = 8;
            // 
            // tb_companyAddr
            // 
            this.tb_companyAddr.Location = new System.Drawing.Point(9, 81);
            this.tb_companyAddr.Name = "tb_companyAddr";
            this.tb_companyAddr.Size = new System.Drawing.Size(359, 22);
            this.tb_companyAddr.TabIndex = 9;
            // 
            // tb_developerName
            // 
            this.tb_developerName.Location = new System.Drawing.Point(9, 125);
            this.tb_developerName.Name = "tb_developerName";
            this.tb_developerName.Size = new System.Drawing.Size(359, 22);
            this.tb_developerName.TabIndex = 10;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.groupBox_TestingClass);
            this.groupBox6.Controls.Add(this.platform);
            this.groupBox6.Controls.Add(this.tb_appVersion);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.tb_appSHA1);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.tb_appId);
            this.groupBox6.Controls.Add(this.tb_appCommonName);
            this.groupBox6.Location = new System.Drawing.Point(6, 222);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(374, 271);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "APP資訊";
            // 
            // groupBox_TestingClass
            // 
            this.groupBox_TestingClass.Controls.Add(this.rb_class3);
            this.groupBox_TestingClass.Controls.Add(this.rb_class1);
            this.groupBox_TestingClass.Controls.Add(this.rb_class2);
            this.groupBox_TestingClass.Location = new System.Drawing.Point(144, 17);
            this.groupBox_TestingClass.Name = "groupBox_TestingClass";
            this.groupBox_TestingClass.Size = new System.Drawing.Size(164, 46);
            this.groupBox_TestingClass.TabIndex = 19;
            this.groupBox_TestingClass.TabStop = false;
            this.groupBox_TestingClass.Text = "檢測分級";
            // 
            // rb_class3
            // 
            this.rb_class3.AutoSize = true;
            this.rb_class3.Location = new System.Drawing.Point(112, 21);
            this.rb_class3.Name = "rb_class3";
            this.rb_class3.Size = new System.Drawing.Size(47, 16);
            this.rb_class3.TabIndex = 18;
            this.rb_class3.TabStop = true;
            this.rb_class3.Tag = "第三類,高級";
            this.rb_class3.Text = "丙級";
            this.rb_class3.UseVisualStyleBackColor = true;
            this.rb_class3.CheckedChanged += new System.EventHandler(this.Rb_class3_CheckedChanged);
            // 
            // rb_class1
            // 
            this.rb_class1.AutoSize = true;
            this.rb_class1.Location = new System.Drawing.Point(6, 21);
            this.rb_class1.Name = "rb_class1";
            this.rb_class1.Size = new System.Drawing.Size(47, 16);
            this.rb_class1.TabIndex = 16;
            this.rb_class1.TabStop = true;
            this.rb_class1.Tag = "第一類,初級";
            this.rb_class1.Text = "甲級";
            this.rb_class1.UseVisualStyleBackColor = true;
            this.rb_class1.CheckedChanged += new System.EventHandler(this.Rb_class1_CheckedChanged);
            // 
            // rb_class2
            // 
            this.rb_class2.AutoSize = true;
            this.rb_class2.Location = new System.Drawing.Point(59, 21);
            this.rb_class2.Name = "rb_class2";
            this.rb_class2.Size = new System.Drawing.Size(47, 16);
            this.rb_class2.TabIndex = 17;
            this.rb_class2.TabStop = true;
            this.rb_class2.Tag = "第二類,中級";
            this.rb_class2.Text = "乙級";
            this.rb_class2.UseVisualStyleBackColor = true;
            this.rb_class2.CheckedChanged += new System.EventHandler(this.Rb_class2_CheckedChanged);
            // 
            // platform
            // 
            this.platform.Controls.Add(this.rb_android);
            this.platform.Controls.Add(this.rb_iOS);
            this.platform.Location = new System.Drawing.Point(6, 17);
            this.platform.Name = "platform";
            this.platform.Size = new System.Drawing.Size(120, 46);
            this.platform.TabIndex = 18;
            this.platform.TabStop = false;
            this.platform.Text = "平台";
            // 
            // rb_android
            // 
            this.rb_android.AutoSize = true;
            this.rb_android.Location = new System.Drawing.Point(6, 21);
            this.rb_android.Name = "rb_android";
            this.rb_android.Size = new System.Drawing.Size(62, 16);
            this.rb_android.TabIndex = 16;
            this.rb_android.TabStop = true;
            this.rb_android.Text = "Android";
            this.rb_android.UseVisualStyleBackColor = true;
            // 
            // rb_iOS
            // 
            this.rb_iOS.AutoSize = true;
            this.rb_iOS.Location = new System.Drawing.Point(74, 21);
            this.rb_iOS.Name = "rb_iOS";
            this.rb_iOS.Size = new System.Drawing.Size(40, 16);
            this.rb_iOS.TabIndex = 17;
            this.rb_iOS.TabStop = true;
            this.rb_iOS.Text = "iOS";
            this.rb_iOS.UseVisualStyleBackColor = true;
            // 
            // tb_appVersion
            // 
            this.tb_appVersion.Location = new System.Drawing.Point(6, 245);
            this.tb_appVersion.Name = "tb_appVersion";
            this.tb_appVersion.Size = new System.Drawing.Size(359, 22);
            this.tb_appVersion.TabIndex = 15;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("新細明體", 12F);
            this.label11.Location = new System.Drawing.Point(3, 226);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(115, 16);
            this.label11.TabIndex = 14;
            this.label11.Text = "APP程式版本：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("新細明體", 12F);
            this.label6.Location = new System.Drawing.Point(3, 94);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 16);
            this.label6.TabIndex = 3;
            this.label6.Text = "APP通用名稱：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("新細明體", 12F);
            this.label7.Location = new System.Drawing.Point(3, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 16);
            this.label7.TabIndex = 4;
            this.label7.Text = "APP唯一識別名稱：";
            // 
            // tb_appSHA1
            // 
            this.tb_appSHA1.Location = new System.Drawing.Point(6, 201);
            this.tb_appSHA1.Name = "tb_appSHA1";
            this.tb_appSHA1.Size = new System.Drawing.Size(359, 22);
            this.tb_appSHA1.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("新細明體", 12F);
            this.label8.Location = new System.Drawing.Point(3, 182);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 16);
            this.label8.TabIndex = 5;
            this.label8.Text = "SHA1：";
            // 
            // tb_appId
            // 
            this.tb_appId.Location = new System.Drawing.Point(6, 157);
            this.tb_appId.Name = "tb_appId";
            this.tb_appId.Size = new System.Drawing.Size(359, 22);
            this.tb_appId.TabIndex = 12;
            // 
            // tb_appCommonName
            // 
            this.tb_appCommonName.Location = new System.Drawing.Point(6, 113);
            this.tb_appCommonName.Name = "tb_appCommonName";
            this.tb_appCommonName.Size = new System.Drawing.Size(359, 22);
            this.tb_appCommonName.TabIndex = 11;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(907, 22);
            this.statusStrip1.TabIndex = 15;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(31, 17);
            this.toolStripStatusLabel1.Text = "編號";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(55, 17);
            this.toolStripStatusLabel2.Text = "基準標題";
            // 
            // openJsonrDialog
            // 
            this.openJsonrDialog.FileName = "openFileDialog1";
            this.openJsonrDialog.Filter = "jsonr檔案|*.jsonr";
            this.openJsonrDialog.InitialDirectory = "saves";
            this.openJsonrDialog.RestoreDirectory = true;
            // 
            // saveJsonrDialog
            // 
            this.saveJsonrDialog.DefaultExt = "jsonr";
            this.saveJsonrDialog.Filter = "jsonr檔案|*.jsonr";
            this.saveJsonrDialog.InitialDirectory = "saves";
            this.saveJsonrDialog.Title = "儲存jsonr";
            // 
            // openApplicationDialog
            // 
            this.openApplicationDialog.FileName = "openFileDialog1";
            // 
            // saveDocxDialog
            // 
            this.saveDocxDialog.DefaultExt = "docx";
            this.saveDocxDialog.Filter = "Word檔|*.docx";
            this.saveDocxDialog.Title = "輸出docx";
            // 
            // btn_toolBox
            // 
            this.btn_toolBox.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.btn_toolBox.FlatAppearance.BorderSize = 2;
            this.btn_toolBox.Location = new System.Drawing.Point(258, 73);
            this.btn_toolBox.Name = "btn_toolBox";
            this.btn_toolBox.Size = new System.Drawing.Size(77, 26);
            this.btn_toolBox.TabIndex = 14;
            this.btn_toolBox.Text = "檢測工具";
            this.btn_toolBox.UseVisualStyleBackColor = true;
            this.btn_toolBox.Click += new System.EventHandler(this.Btn_toolBox_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 562);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.gb_subRule1.ResumeLayout(false);
            this.gb_subRule1.PerformLayout();
            this.gb_subRule2.ResumeLayout(false);
            this.gb_subRule2.PerformLayout();
            this.gb_subRule7.ResumeLayout(false);
            this.gb_subRule7.PerformLayout();
            this.gb_subRule3.ResumeLayout(false);
            this.gb_subRule3.PerformLayout();
            this.gb_subRule6.ResumeLayout(false);
            this.gb_subRule6.PerformLayout();
            this.gb_subRule4.ResumeLayout(false);
            this.gb_subRule4.PerformLayout();
            this.gb_subRule5.ResumeLayout(false);
            this.gb_subRule5.PerformLayout();
            this.functions.ResumeLayout(false);
            this.functions.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox_TestingClass.ResumeLayout(false);
            this.groupBox_TestingClass.PerformLayout();
            this.platform.ResumeLayout(false);
            this.platform.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox functions;
        private System.Windows.Forms.Label label_finalResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_notFit;
        private System.Windows.Forms.Button btn_fail;
        private System.Windows.Forms.Button btn_accept;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.GroupBox gb_subRule3;
        private System.Windows.Forms.RadioButton rb_rule3Fail;
        private System.Windows.Forms.RadioButton rb_rule3Accept;
        private System.Windows.Forms.TextBox detail3;
        private System.Windows.Forms.GroupBox gb_subRule1;
        private System.Windows.Forms.RadioButton rb_rule1Fail;
        private System.Windows.Forms.RadioButton rb_rule1Accept;
        private System.Windows.Forms.TextBox detail1;
        private System.Windows.Forms.GroupBox gb_subRule2;
        private System.Windows.Forms.RadioButton rb_rule2Fail;
        private System.Windows.Forms.RadioButton rb_rule2Accept;
        private System.Windows.Forms.TextBox detail2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tb_appVersion;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_appSHA1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_appId;
        private System.Windows.Forms.TextBox tb_appCommonName;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_companyName;
        private System.Windows.Forms.TextBox tb_companyAddr;
        private System.Windows.Forms.TextBox tb_developerName;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DateTimePicker dtp_finishDate;
        private System.Windows.Forms.DateTimePicker dtp_startDate;
        private System.Windows.Forms.TextBox tb_reportVersion;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker dtp_reportDate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tb_reportNo;
        private System.Windows.Forms.Button btn_saveJsonr;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.Button btn_outputDocx;
        private System.Windows.Forms.Button btn_loadJsonr;
        private System.Windows.Forms.OpenFileDialog openJsonrDialog;
        private System.Windows.Forms.SaveFileDialog saveJsonrDialog;
        private System.Windows.Forms.TextBox tb_outputDetail;
        private System.Windows.Forms.Button btn_saveNewJsonr;
        private System.Windows.Forms.Button btn_newJsonr;
        private System.Windows.Forms.Button btn_readApplication;
        private System.Windows.Forms.OpenFileDialog openApplicationDialog;
        private System.Windows.Forms.SaveFileDialog saveDocxDialog;
        private System.Windows.Forms.RadioButton rb_iOS;
        private System.Windows.Forms.RadioButton rb_android;
        private System.Windows.Forms.GroupBox gb_subRule4;
        private System.Windows.Forms.RadioButton rb_rule4Fail;
        private System.Windows.Forms.RadioButton rb_rule4Accept;
        private System.Windows.Forms.TextBox detail4;
        private System.Windows.Forms.GroupBox gb_subRule5;
        private System.Windows.Forms.RadioButton rb_rule5Fail;
        private System.Windows.Forms.RadioButton rb_rule5Accept;
        private System.Windows.Forms.TextBox detail5;
        private System.Windows.Forms.GroupBox gb_subRule6;
        private System.Windows.Forms.RadioButton rb_rule6Fail;
        private System.Windows.Forms.RadioButton rb_rule6Accept;
        private System.Windows.Forms.TextBox detail6;
        private System.Windows.Forms.GroupBox gb_subRule7;
        private System.Windows.Forms.RadioButton rb_rule7Fail;
        private System.Windows.Forms.RadioButton rb_rule7Accept;
        private System.Windows.Forms.TextBox detail7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox_TestingClass;
        private System.Windows.Forms.RadioButton rb_class3;
        private System.Windows.Forms.RadioButton rb_class1;
        private System.Windows.Forms.RadioButton rb_class2;
        private System.Windows.Forms.GroupBox platform;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btn_PassCondition;
        private System.Windows.Forms.Button btn_Picture1;
        private System.Windows.Forms.Button btn_Picture2;
        private System.Windows.Forms.Button btn_Picture7;
        private System.Windows.Forms.Button btn_Picture3;
        private System.Windows.Forms.Button btn_Picture6;
        private System.Windows.Forms.Button btn_Picture4;
        private System.Windows.Forms.Button btn_Picture5;
        private System.Windows.Forms.RichTextBox tb_finalText;
        private System.Windows.Forms.Button btn_toolBox;
    }
}

