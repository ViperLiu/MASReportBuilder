﻿namespace MASReportBuilder
{
    partial class PassConditionWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_ConditionDetail = new System.Windows.Forms.RichTextBox();
            this.btn_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtb_ConditionDetail
            // 
            this.rtb_ConditionDetail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rtb_ConditionDetail.DetectUrls = false;
            this.rtb_ConditionDetail.Location = new System.Drawing.Point(6, 5);
            this.rtb_ConditionDetail.Name = "rtb_ConditionDetail";
            this.rtb_ConditionDetail.ReadOnly = true;
            this.rtb_ConditionDetail.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.rtb_ConditionDetail.Size = new System.Drawing.Size(325, 158);
            this.rtb_ConditionDetail.TabIndex = 0;
            this.rtb_ConditionDetail.Text = "";
            // 
            // btn_Close
            // 
            this.btn_Close.Location = new System.Drawing.Point(120, 169);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(95, 25);
            this.btn_Close.TabIndex = 1;
            this.btn_Close.Text = "關閉";
            this.btn_Close.UseVisualStyleBackColor = true;
            this.btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // PassConditionWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 199);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.rtb_ConditionDetail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PassConditionWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "判定規則";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_ConditionDetail;
        private System.Windows.Forms.Button btn_Close;
    }
}