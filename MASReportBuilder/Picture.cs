﻿using Newtonsoft.Json;
using System.ComponentModel;
using System.Drawing;
namespace MASReportBuilder
{
    public class Picture : INotifyPropertyChanged
    {
        [JsonIgnore]
        public readonly string FullPath;

        public readonly string Path;
        private string _Caption;
        public string Caption
        {
            get { return _Caption; }
            private set
            {
                if(_Caption != value)
                {
                    _Caption = value;
                    OnPropertyChanged("Caption");
                }
            }
        }

        [JsonIgnore]
        public Image Image { get; private set; }
        [JsonIgnore]
        public double Ratio { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public Picture(string path, string caption = "")
        {
            this.Path = path;
            this.FullPath = MASData.ProjectInfo.GetFullPath(ProjectPath.PicturesFolder) + "\\" + path;
            this.Caption = caption;
            this.Ratio = -1;
            this.PropertyChanged += MASData.Changed;
        }

        public void AddCaption(string text)
        {
            this.Caption = text;
        }

        public void CreatePicture()
        {
            this.Image = Image.FromFile(this.FullPath);
            this.Ratio = (double)this.Image.Width / (double)this.Image.Height;
        }

        public void ReleasePictureResource()
        {
            this.Image.Dispose();
            this.Ratio = -1;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
