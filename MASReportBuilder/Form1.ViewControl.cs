﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MASReportBuilder
{
    partial class Form1
    {
        //show result text to textbox
        public void DisplayResultText()
        {
            var rule = MASData.CurrentSelectedRuleResult;
            tb_finalText.Text = "";
            StringBuilder sb = new StringBuilder();
            int subRuleNum = 0;
            foreach(var sub in rule.SubRuleList)
            {
                if (sub.Text == null)
                    continue;
                sb.Append(">>");
                sb.Append(sub.Text);
                sb.Append("\r\n\r\n");
                subRuleNum++;
            }
            tb_finalText.Text = sb.ToString();
            Console.WriteLine(tb_finalText.Text);
        }

        //change treeNode back color
        public void UpdateTreeNodeBackColor(string NumberOfRuleToUpdate)
        {
            if (NumberOfRuleToUpdate == null)
                return;
            if (!MASData.Report.RuleList.TryGetValue(NumberOfRuleToUpdate, out RuleResults RuleToUpdate))
                return;
            string tmp = "Node" + NumberOfRuleToUpdate.Replace(".", "");
            TreeNode SelectedNode;
            try
            {
                SelectedNode = treeView1.Nodes.Find(tmp, true)[0];
            }
            catch (IndexOutOfRangeException)
            {
                //this should not happen
                MessageBox.Show("基準編號有誤");
                return;
            }

            if (SelectedNode == null)
                return;

            SelectedNode.ForeColor = Color.Black;
            SelectedNode.BackColor = Color.White;
            string finalResult = RuleToUpdate.FinalResult;
            if (finalResult == "accept")
            {
                SelectedNode.BackColor = Color.LightGreen;
            }
            else if (finalResult == "fail")
            {
                SelectedNode.BackColor = Color.LightPink;
            }
            else if (finalResult == "notfit")
            {
                SelectedNode.BackColor = Color.LightGray;
            }
            else if (finalResult == "donttest")
            {
                SelectedNode.ForeColor = Color.LightGray;
            }
            else
            {
                SelectedNode.BackColor = Color.White;
            }
        }

        //show result on label
        public void UpdateResultLabel()
        {
            var rule = MASData.Report.RuleList[MASData.CurrentSelectRuleNumber];

            if (rule == null)
                return;

            string tmp = rule.FinalResult;
            if (tmp == "undetermin")
            {
                label_finalResult.Text = "尚未檢測";
                label_finalResult.ForeColor = Color.Black;
            }
            else if (tmp == "accept")
            {
                label_finalResult.Text = "符合";
                label_finalResult.ForeColor = Color.Green;
            }
            else if (tmp == "fail")
            {
                label_finalResult.Text = "不符合";
                label_finalResult.ForeColor = Color.Red;
            }
            else if (tmp == "notfit")
            {
                label_finalResult.Text = "不適用";
                label_finalResult.ForeColor = Color.Gray;
            }
            else if (tmp == "donttest")
            {
                label_finalResult.Text = "不須檢測";
                label_finalResult.ForeColor = Color.Gray;
            }
            else
                label_finalResult.Text = "ERROR !";
        }

        public void ClearRadioButtons()
        {
            for (int i = 0; i < radioButtons.Count; i++)
            {
                SetRadioButtonChecked(radioButtons[i], false);
                radioButtons[i].Enabled = true;
            }
        }

        public void ResetUI()
        {
            //重新設定tree node背景顏色
            for (int i = 0; i < MASData.RuleNumberList.Count; i++)
            {
                UpdateTreeNodeBackColor(MASData.RuleNumberList[i]);
            }

            //清除文字編輯欄位
            tb_finalText.Text = "";

            //清除檢測結果label文字
            label_finalResult.Text = "";

            //清除基準顯示的欄位
            for (int i = 0; i < 7; i++)
            {
                tb_Detail[i].Text = "";
                groups[i].Visible = false;
            }

            //清除radio button
            ClearRadioButtons();
        }

        private void UpdateSubRuleUI()
        {
            //show subrule details
            for (int i = 0; i < groups.Length; i++)
            {
                if (i >= MASData.CurrentSelectedRuleContent.SubRuleContentsList.Count)
                {
                    tb_Detail[i].Text = "";
                    groups[i].Visible = false;
                }
                else
                {
                    tb_Detail[i].Text = MASData.CurrentSelectedRuleContent.SubRuleContentsList[i].Description;
                    groups[i].Visible = true;

                    var check1 = MASData.CurrentSelectedRuleResult.SubRuleList[i].Result == "accept";
                    var check2 = MASData.CurrentSelectedRuleResult.SubRuleList[i].Result == "fail";
                    SetRadioButtonChecked(radioButtons[i * 2], check1);
                    SetRadioButtonChecked(radioButtons[i * 2 + 1], check2);
                }
            }
        }

        private void UpdateNotFitBtn()
        {
            var ruleContents = MASData.RuleContents[MASData.CurrentSelectRuleNumber];
            if (ruleContents.NotFitText == "None")
                btn_notFit.Enabled = false;
            else
                btn_notFit.Enabled = true;
        }

        private void UpdateDontTestUI()
        {
            var ruleResult = MASData.Report.RuleList[MASData.CurrentSelectRuleNumber];
            if (ruleResult.FinalResult == "donttest")
            {
                btn_accept.Enabled = false;
                btn_fail.Enabled = false;
                btn_notFit.Enabled = false;
                panel1.Enabled = false;
            }
            else
            {
                btn_accept.Enabled = true;
                btn_fail.Enabled = true;
                btn_notFit.Enabled = true;
                panel1.Enabled = true;
            }
        }

        private void UpdateToolStrip()
        {
            this.toolStripStatusLabel1.Text = MASData.CurrentSelectRuleNumber;
            this.toolStripStatusLabel2.Text = MASData.CurrentSelectedRuleContent.Title;
        }

        private void UpdatePictureBtnText()
        {
            var currentRule = MASData.CurrentSelectedRuleResult;
            for (var i = 0; i < currentRule.SubRuleList.Count; i++)
            {
                pictureButtons[i].Text = "圖片(" + currentRule.SubRuleList[i].Pictures.Count.ToString() + ")";
            }
        }

        private void UpdateTreeNodeForClass()
        {
            foreach (var p in MASData.RuleContents)
            {
                var rule = p.Value;
                var number = p.Key;
                //甲級
                if (MASData.Report.Class == 0)
                {
                    if (rule.Class != 1 && rule.Class != 3 && rule.Class != 5 && rule.Class != 7)
                    {
                        MASData.Report.RuleList[number].DontTest();
                        UpdateTreeNodeBackColor(p.Key);
                    }
                    else if (MASData.Report.RuleList[number].FinalResult == "donttest")
                    {
                        MASData.Report.RuleList[number].Reset();
                        UpdateTreeNodeBackColor(p.Key);
                    }
                }
                //乙級
                else if (MASData.Report.Class == 1)
                {
                    if (rule.Class != 2 && rule.Class != 3 && rule.Class != 6 && rule.Class != 7)
                    {
                        MASData.Report.RuleList[number].DontTest();
                        UpdateTreeNodeBackColor(p.Key);
                    }
                    else if (MASData.Report.RuleList[number].FinalResult == "donttest")
                    {
                        MASData.Report.RuleList[number].Reset();
                        UpdateTreeNodeBackColor(p.Key);
                    }
                }
                //丙級
                else if (MASData.Report.Class == 2)
                {
                    if (rule.Class != 4 && rule.Class != 5 && rule.Class != 6 && rule.Class != 7)
                    {
                        MASData.Report.RuleList[number].DontTest();
                        UpdateTreeNodeBackColor(p.Key);
                    }
                    else if (MASData.Report.RuleList[number].FinalResult == "donttest")
                    {
                        MASData.Report.RuleList[number].Reset();
                        UpdateTreeNodeBackColor(p.Key);
                    }
                }

            }
        }
    }
}
