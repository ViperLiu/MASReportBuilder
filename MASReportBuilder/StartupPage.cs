﻿using System;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace MASReportBuilder
{
    public partial class StartupPage : Form
    {
        //public ProjectInfo Info;
        Form1 main = new Form1();
        

        public StartupPage()
        {
            InitializeComponent();
        }

        public string GetSHA1(string targetFile)
        {
            if (targetFile == "" || targetFile == null)
                return "";
            byte[] apkByte = File.ReadAllBytes(targetFile);
            SHA1 sha1 = new SHA1CryptoServiceProvider();//建立一個SHA1
            byte[] crypto = sha1.ComputeHash(apkByte);//進行SHA1加密
            string result = BitConverter.ToString(crypto);//把加密後的字串從Byte[]轉為字串
            result = result.Replace("-", "").ToLower();
            return result;
        }

        private void Btn_creatNewProject_Click(object sender, EventArgs e)
        {
            if (!CheckUserInput())
                return;

            var projectName = tb_projectName.Text;

            var result = projectLocationDialog.ShowDialog();
            if (result != DialogResult.OK)
                return;

            var projectLocation = CreateProjectFolder(projectName, projectLocationDialog.SelectedPath);


            SetConfigs(projectLocation);//取得使用者輸入的設定值
            CopyApplication();//將使用者指定的申請書複製到專案資料夾底下
            CopyInstaller();//將使用者指定的安裝檔複製到專案資料夾底下
            SaveConfig();//儲存設定值

            //隱藏啟動畫面
            this.Hide();
            
            try
            {
                main.SetSHA1(MASData.ProjectInfo.InstallerSHA1);
                main.SetClass(MASData.ProjectInfo.Class);
                main.SetAppOS(MASData.ProjectInfo.OS);
                main.SaveFile(MASData.ProjectInfo.GetFullPath(ProjectPath.Jsonr));
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                main.Close();
                this.Show();
                return;
            }
            main.ShowDialog();
            this.Close();
        }

        private void Btn_application_Click(object sender, EventArgs e)
        {
            MASData.ProjectInfo.OS = rb_Android.Checked ? "android" : "ios";
            openFileDialog1.Filter = "Word檔|*.docx";
            var result = openFileDialog1.ShowDialog();
            if (result != DialogResult.OK)
                return;

            var file = openFileDialog1.FileName;
            var extend = Path.GetExtension(file).ToLower();
            if(extend != ".docx")
            {
                MessageBox.Show("不支援此檔案格式，只支援.docx");
                return;
            }

            try
            {
                main.ReadApplication(file);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            var btn = sender as Button;
            btn.FlatAppearance.BorderColor = Color.LightGreen;
            btn.Text = "申請書(已指定)";
            btn.Tag = file;
        }

        private void Btn_installer_Click(object sender, EventArgs e)
        {
            MASData.ProjectInfo.OS = rb_Android.Checked ? "android" : "ios";
            openFileDialog1.Filter = "所有檔案|*.*|apk檔|*.apk|ipa檔|*.ipa";
            var result = openFileDialog1.ShowDialog();
            if (result != DialogResult.OK)
                return;

            var file = openFileDialog1.FileName;
            var extend = Path.GetExtension(file).ToLower();

            var isExtMatchAndroid = (MASData.ProjectInfo.OS == "android" && extend == ".apk");
            var isExtMatchiOS = (MASData.ProjectInfo.OS == "ios" && extend == ".ipa");

            if (extend != ".apk" && extend != ".ipa")
            {
                MessageBox.Show("不支援此檔案格式，只支援.apk或.ipa");
                return;
            }

            if (!isExtMatchAndroid && !isExtMatchiOS)
            {
                MessageBox.Show(
                    string.Format("你指定的安裝檔({0})，不符合你選擇的作業系統({1})",
                    extend, MASData.ProjectInfo.OS)
                    );
                return;
            }

            var btn = sender as Button;
            btn.FlatAppearance.BorderColor = Color.LightGreen;
            btn.Text = "安裝檔(已指定)";
            btn.Tag = file;
        }

        private void Btn_openProject_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "MAS報告|*.jsonr";
            var result = openFileDialog1.ShowDialog();
            if (result != DialogResult.OK)
                return;
            var jsonrName = openFileDialog1.FileName;
            var projectFolder = Path.GetDirectoryName(jsonrName);
            var config = projectFolder + "\\config";
            if (!File.Exists(config))
            {
                MessageBox.Show("找不到 " + config, "找不到config檔！");
                return;
            }
            LoadConfig(config);
            CheckConfig(projectFolder);
            this.Hide();
            main.LoadFile(jsonrName);
            main.ShowDialog();
            this.Close();
        }

        private void SaveConfig()
        {
            string output = JsonConvert.SerializeObject(MASData.ProjectInfo);
            File.WriteAllText(MASData.ProjectInfo.ProjectFolder + "\\config", output);
        }

        private void LoadConfig(string config)
        {
            string json = "";
            using (var r = new StreamReader(config))
            {
                json = r.ReadToEnd();
            }
            MASData.ProjectInfo = JsonConvert.DeserializeObject<ProjectInfo>(json);
        }

        private void CheckConfig(string projectFolder)
        {
            MASData.ProjectInfo.ProjectFolder = projectFolder;
            SaveConfig();

        }

        private string GenerateJsonrName()
        {
            var projectName = tb_projectName.Text;
            DateTime dt = DateTime.Now;
            var date = dt.Year.ToString() + dt.Month.ToString().PadLeft(2, '0') + dt.Day.ToString().PadLeft(2, '0');
            var jsonrName = string.Format("{0}_{1}_檢測報告_{2}.jsonr", projectName, MASData.ProjectInfo.OS, date);
            return jsonrName;
        }

        private bool CheckUserInput()
        {
            if (tb_projectName.Text == "")
                return false;

            if (!rb_class0.Checked && !rb_class1.Checked && !rb_class2.Checked)
                return false;

            if (!rb_Android.Checked && !rb_iOS.Checked)
                return false;

            return true;
        }

        private string CreateProjectFolder(string projectName, string targetPath)
        {
            var projectLocation = targetPath + "\\" + projectName;
            var pictureSubfolder = projectLocation + "\\pictures";

            if (Directory.Exists(projectLocation))
            {
                MessageBox.Show("已存在同名的資料夾，請更改專案名稱");
                return null;
            }

            //新增專案資料夾與圖片資料夾
            Directory.CreateDirectory(projectLocation);
            Directory.CreateDirectory(pictureSubfolder);

            return projectLocation;
        }

        private void CopyApplication()
        {
            var applicationPathOld = btn_application.Tag != null ? btn_application.Tag.ToString() : "";
            if (applicationPathOld == "")
                return;
            var applicationName = Path.GetFileName(applicationPathOld);
            var applicationPathNew = MASData.ProjectInfo.ProjectFolder + "\\" + applicationName;
            File.Copy(applicationPathOld, applicationPathNew);
            MASData.ProjectInfo.ApplicationPath = applicationName;
        }

        private void CopyInstaller()
        {
            var installerPathOld = btn_installer.Tag != null ? btn_installer.Tag.ToString() : "";
            if (installerPathOld == "")
                return;
            var installerName = Path.GetFileName(installerPathOld);
            var installerPathNew = MASData.ProjectInfo.ProjectFolder + "\\" + installerName;
            File.Copy(installerPathOld, installerPathNew);
            MASData.ProjectInfo.InstallerPath = installerName;
            MASData.ProjectInfo.InstallerSHA1 = GetSHA1(installerPathNew);
        }

        private void SetConfigs(string projectLocation)
        {
            MASData.ProjectInfo.OS = rb_Android.Checked ? "android" : "ios";

            if (rb_class0.Checked) MASData.ProjectInfo.Class = 0;
            else if (rb_class1.Checked) MASData.ProjectInfo.Class = 1;
            else MASData.ProjectInfo.Class = 2;

            MASData.ProjectInfo.ProjectFolder = projectLocation;

            var jsonrName = GenerateJsonrName();
            MASData.ProjectInfo.JsonrPath = jsonrName;
        }

        private void InfoDebug()
        {
            Console.WriteLine(MASData.ProjectInfo.OS);
            Console.WriteLine(MASData.ProjectInfo.Class);
            Console.WriteLine(MASData.ProjectInfo.PicturesFolder);
            Console.WriteLine(MASData.ProjectInfo.ProjectFolder);
            Console.WriteLine(MASData.ProjectInfo.ApplicationPath);
            Console.WriteLine(MASData.ProjectInfo.InstallerPath);
            Console.WriteLine(MASData.ProjectInfo.InstallerSHA1);
            Console.WriteLine(MASData.ProjectInfo.JsonrPath);
        }
    }
}
