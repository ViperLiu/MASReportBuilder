﻿namespace MASReportBuilder
{
    partial class StartupPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_creatNewProject = new System.Windows.Forms.Button();
            this.btn_openProject = new System.Windows.Forms.Button();
            this.tb_projectName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.projectLocationDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rb_iOS = new System.Windows.Forms.RadioButton();
            this.rb_Android = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rb_class2 = new System.Windows.Forms.RadioButton();
            this.rb_class1 = new System.Windows.Forms.RadioButton();
            this.rb_class0 = new System.Windows.Forms.RadioButton();
            this.btn_application = new System.Windows.Forms.Button();
            this.btn_installer = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_creatNewProject
            // 
            this.btn_creatNewProject.Location = new System.Drawing.Point(12, 173);
            this.btn_creatNewProject.Name = "btn_creatNewProject";
            this.btn_creatNewProject.Size = new System.Drawing.Size(224, 114);
            this.btn_creatNewProject.TabIndex = 0;
            this.btn_creatNewProject.Text = "建立新專案";
            this.btn_creatNewProject.UseVisualStyleBackColor = true;
            this.btn_creatNewProject.Click += new System.EventHandler(this.Btn_creatNewProject_Click);
            // 
            // btn_openProject
            // 
            this.btn_openProject.Location = new System.Drawing.Point(12, 342);
            this.btn_openProject.Name = "btn_openProject";
            this.btn_openProject.Size = new System.Drawing.Size(224, 114);
            this.btn_openProject.TabIndex = 1;
            this.btn_openProject.Text = "開啟現有專案";
            this.btn_openProject.UseVisualStyleBackColor = true;
            this.btn_openProject.Click += new System.EventHandler(this.Btn_openProject_Click);
            // 
            // tb_projectName
            // 
            this.tb_projectName.Location = new System.Drawing.Point(12, 24);
            this.tb_projectName.Name = "tb_projectName";
            this.tb_projectName.Size = new System.Drawing.Size(224, 22);
            this.tb_projectName.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "輸入專案名稱";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rb_iOS);
            this.panel1.Controls.Add(this.rb_Android);
            this.panel1.Location = new System.Drawing.Point(12, 87);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(224, 28);
            this.panel1.TabIndex = 4;
            // 
            // rb_iOS
            // 
            this.rb_iOS.AutoSize = true;
            this.rb_iOS.Location = new System.Drawing.Point(71, 3);
            this.rb_iOS.Name = "rb_iOS";
            this.rb_iOS.Size = new System.Drawing.Size(40, 16);
            this.rb_iOS.TabIndex = 4;
            this.rb_iOS.Text = "iOS";
            this.rb_iOS.UseVisualStyleBackColor = true;
            // 
            // rb_Android
            // 
            this.rb_Android.AutoSize = true;
            this.rb_Android.Checked = true;
            this.rb_Android.Location = new System.Drawing.Point(3, 3);
            this.rb_Android.Name = "rb_Android";
            this.rb_Android.Size = new System.Drawing.Size(62, 16);
            this.rb_Android.TabIndex = 3;
            this.rb_Android.TabStop = true;
            this.rb_Android.Text = "Android";
            this.rb_Android.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.rb_class2);
            this.panel2.Controls.Add(this.rb_class1);
            this.panel2.Controls.Add(this.rb_class0);
            this.panel2.Location = new System.Drawing.Point(12, 55);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(224, 26);
            this.panel2.TabIndex = 5;
            // 
            // rb_class2
            // 
            this.rb_class2.AutoSize = true;
            this.rb_class2.Location = new System.Drawing.Point(109, 5);
            this.rb_class2.Name = "rb_class2";
            this.rb_class2.Size = new System.Drawing.Size(47, 16);
            this.rb_class2.TabIndex = 2;
            this.rb_class2.Text = "丙類";
            this.rb_class2.UseVisualStyleBackColor = true;
            // 
            // rb_class1
            // 
            this.rb_class1.AutoSize = true;
            this.rb_class1.Location = new System.Drawing.Point(56, 5);
            this.rb_class1.Name = "rb_class1";
            this.rb_class1.Size = new System.Drawing.Size(47, 16);
            this.rb_class1.TabIndex = 1;
            this.rb_class1.Text = "乙類";
            this.rb_class1.UseVisualStyleBackColor = true;
            // 
            // rb_class0
            // 
            this.rb_class0.AutoSize = true;
            this.rb_class0.Checked = true;
            this.rb_class0.Location = new System.Drawing.Point(3, 5);
            this.rb_class0.Name = "rb_class0";
            this.rb_class0.Size = new System.Drawing.Size(47, 16);
            this.rb_class0.TabIndex = 0;
            this.rb_class0.TabStop = true;
            this.rb_class0.Text = "甲類";
            this.rb_class0.UseVisualStyleBackColor = true;
            // 
            // btn_application
            // 
            this.btn_application.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_application.FlatAppearance.BorderSize = 2;
            this.btn_application.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_application.Location = new System.Drawing.Point(12, 122);
            this.btn_application.Name = "btn_application";
            this.btn_application.Size = new System.Drawing.Size(103, 45);
            this.btn_application.TabIndex = 6;
            this.btn_application.Text = "申請書(未指定)";
            this.btn_application.UseVisualStyleBackColor = true;
            this.btn_application.Click += new System.EventHandler(this.Btn_application_Click);
            // 
            // btn_installer
            // 
            this.btn_installer.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_installer.FlatAppearance.BorderSize = 2;
            this.btn_installer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_installer.Location = new System.Drawing.Point(133, 122);
            this.btn_installer.Name = "btn_installer";
            this.btn_installer.Size = new System.Drawing.Size(103, 45);
            this.btn_installer.TabIndex = 7;
            this.btn_installer.Text = "安裝檔(未指定)";
            this.btn_installer.UseVisualStyleBackColor = true;
            this.btn_installer.Click += new System.EventHandler(this.Btn_installer_Click);
            // 
            // StartupPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 468);
            this.Controls.Add(this.btn_installer);
            this.Controls.Add(this.btn_application);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_projectName);
            this.Controls.Add(this.btn_openProject);
            this.Controls.Add(this.btn_creatNewProject);
            this.Name = "StartupPage";
            this.Text = "StartupPage";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_creatNewProject;
        private System.Windows.Forms.Button btn_openProject;
        private System.Windows.Forms.TextBox tb_projectName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog projectLocationDialog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rb_iOS;
        private System.Windows.Forms.RadioButton rb_Android;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rb_class2;
        private System.Windows.Forms.RadioButton rb_class1;
        private System.Windows.Forms.RadioButton rb_class0;
        private System.Windows.Forms.Button btn_application;
        private System.Windows.Forms.Button btn_installer;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}