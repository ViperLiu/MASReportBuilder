﻿using System;
using System.IO;
using System.Windows.Forms;

namespace MASReportBuilder
{
    partial class Form1
    {
        public void SetSHA1(string sha1)
        {
            MASData.Report.AppSHA1 = sha1;
        }

        public void SetAppOS(string os)
        {
            if (os == "android")
                rb_android.Checked = true;
            else if (os == "ios")
                rb_iOS.Checked = true;
            else
                throw new Exception("只接受\"android\"或是\"ios\"");
            MASData.Report.AppOS = os;
        }

        public void SetClass(int Class)
        {
            MASData.Report.Class = Class;
            comboBox1.SelectedIndex = Class;

            if (MASData.Report.Class == 0)
            {
                rb_class1.Checked = true;
            }
            else if (MASData.Report.Class == 1)
                rb_class2.Checked = true;
            else
                rb_class3.Checked = true;

            UpdateTreeNodeForClass();
        }

        public void LoadFile(string file)
        {
            try
            {
                JsonFileController json = new JsonFileController(file);
                MASData.Report = json.LoadFile();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("無法開啟檔案，檔案格式錯誤");
                return;
            }

            ReportDataSource.DataSource = MASData.Report;
            MASData.Report.CurrentOpenedFile = file;
        }

        public void SaveFile(string file)
        {
            Console.WriteLine("saved : " + file);
            JsonFileController json = new JsonFileController(file);
            json.SaveFile(MASData.Report);
            MASData.Report.MarkAsSaved();
            MASData.Report.CurrentOpenedFile = file;
        }

        private void SaveAsNewFile()
        {
            var result = saveJsonrDialog.ShowDialog();
            var file = saveJsonrDialog.FileName;
            if (result == DialogResult.OK)
            {
                SaveFile(file);
                MASData.Report.CurrentOpenedFile = file;
            }
        }
        
        public void BuildData()
        {
            if (!LoadRuleNumber())
                MessageBox.Show("載入 基準編號 時發生錯誤");
            try
            {
                //新的報告
                MASData.Report = new Report();
                MASData.Report.GetDates(
                dtp_startDate.Value.ToLongDateString(),
                dtp_finishDate.Value.ToLongDateString(),
                dtp_reportDate.Value.ToLongDateString()
                );
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                MessageBox.Show(e.StackTrace);
                MessageBox.Show("載入 基準內容 時發生錯誤");
            }

            //新的資料來源
            ReportDataSource.DataSource = MASData.Report;
        }

        private bool LoadRuleNumber()
        {
            try
            {
                using (StreamReader r = new StreamReader("assets\\rule list30.txt"))
                {
                    string line;
                    while ((line = r.ReadLine()) != null)
                    {
                        MASData.RuleNumberList.Add(line);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                MessageBox.Show(e.StackTrace);
                return false;
            }
        }

        public bool SelectRule(string selectedRules)
        {
            //record current selected rule and rule number
            if (!MASData.Report.RuleList.TryGetValue(selectedRules, out RuleResults rule))
            {
                MASData.CurrentSelectRuleNumber = null;
                MASData.CurrentSelectedRuleContent = null;
                MASData.CurrentSelectedRuleResult = null;
                return false;
            }
            MASData.CurrentSelectRuleNumber = selectedRules;
            MASData.CurrentSelectedRuleContent = MASData.RuleContents[selectedRules];
            MASData.CurrentSelectedRuleResult = MASData.Report.RuleList[selectedRules];
            return true;
        }

        public void SaveResults()
        {
            if (MASData.CurrentSelectedRuleResult == null)
                return;

            MASData.CurrentSelectedRuleResult.SaveResultText(tb_finalText.Text);
        }

        //binding event to radio button
        private void BindEventToRadioButton()
        {
            for (var i = 0; i < radioButtons.Count; i++)
            {
                radioButtons[i].CheckedChanged += new EventHandler(Rb_ruleCheckedChanged);
            }
        }

        //binding event to Picture button
        private void BindEventToPictureButton()
        {
            for (var i = 0; i < pictureButtons.Length; i++)
            {
                pictureButtons[i].Click += new EventHandler(Btn_PictureButton_Click);
            }
        }

        private void SetRadioButtonChecked(RadioButton rb, bool checkedOn)
        {
            this.SuppressRadioButtonEvent = true;
            rb.Checked = checkedOn;
            this.SuppressRadioButtonEvent = false;
        }
    }
}
