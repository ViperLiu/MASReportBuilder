﻿using System;
using System.Windows.Forms;

namespace MASReportBuilder
{
    public partial class PassConditionWindow : Form
    {
        public PassConditionWindow()
        {
            InitializeComponent();
            rtb_ConditionDetail.Text = MASData.CurrentSelectedRuleContent.PassCondition;
            this.Text = MASData.CurrentSelectRuleNumber + " 判定規則";
        }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
