﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace MASReportBuilder
{
    public class SubRuleResult : INotifyPropertyChanged
    {
        private string _Result;
        public string Result
        {
            get { return _Result; }
            set
            {
                if (_Result != value)
                {
                    _Result = value;
                    OnPropertyChanged("Result");
                }
            }
        }

        private string _Text;
        public string Text
        {
            get { return _Text; }
            set
            {
                if (_Text != value)
                {
                    _Text = value;
                    OnPropertyChanged("Text");
                }
            }
        }

        public ObservableCollection<Picture> Pictures { get; private set; }

        public SubRuleResult()
        {
            this.Result = "undetermin";
            this.Text = "";
            this.Pictures = new ObservableCollection<Picture>();
            this.Pictures.CollectionChanged += Pictures_CollectionChanged;
            this.PropertyChanged += MASData.Changed;
        }

        private void Pictures_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged("Pictures");
        }

        public void Accept()
        {
            this.Result = "accept";
        }

        public void Fail()
        {
            this.Result = "fail";
        }

        public void NotFit()
        {
            this.Result = "notfit";
        }

        public void Reset()
        {
            this.Result = "undetermin";
            this.Text = "";
            this.Pictures.Clear();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
