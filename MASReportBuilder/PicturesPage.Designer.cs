﻿namespace MASReportBuilder
{
    partial class PicturesPage
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pb_large = new System.Windows.Forms.PictureBox();
            this.tb_caption = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_addPicture = new System.Windows.Forms.Button();
            this.btn_savePicture = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.selectPictureDialog = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ThumbnailContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ToolStripMenuItem_DeletPicture = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripMenuItem_switchUp = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStripMenuItem_switchDown = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pb_large)).BeginInit();
            this.panel1.SuspendLayout();
            this.ThumbnailContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // pb_large
            // 
            this.pb_large.ImageLocation = "";
            this.pb_large.Location = new System.Drawing.Point(2, 3);
            this.pb_large.Name = "pb_large";
            this.pb_large.Size = new System.Drawing.Size(540, 356);
            this.pb_large.TabIndex = 0;
            this.pb_large.TabStop = false;
            // 
            // tb_caption
            // 
            this.tb_caption.Font = new System.Drawing.Font("新細明體", 12F);
            this.tb_caption.Location = new System.Drawing.Point(164, 398);
            this.tb_caption.Multiline = true;
            this.tb_caption.Name = "tb_caption";
            this.tb_caption.Size = new System.Drawing.Size(540, 109);
            this.tb_caption.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F);
            this.label1.Location = new System.Drawing.Point(164, 379);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "圖片文字說明：";
            // 
            // btn_addPicture
            // 
            this.btn_addPicture.Location = new System.Drawing.Point(523, 513);
            this.btn_addPicture.Name = "btn_addPicture";
            this.btn_addPicture.Size = new System.Drawing.Size(85, 25);
            this.btn_addPicture.TabIndex = 4;
            this.btn_addPicture.Text = "加入圖片";
            this.btn_addPicture.UseVisualStyleBackColor = true;
            this.btn_addPicture.Click += new System.EventHandler(this.Btn_addPicture_Click);
            // 
            // btn_savePicture
            // 
            this.btn_savePicture.Location = new System.Drawing.Point(614, 513);
            this.btn_savePicture.Name = "btn_savePicture";
            this.btn_savePicture.Size = new System.Drawing.Size(85, 25);
            this.btn_savePicture.TabIndex = 5;
            this.btn_savePicture.Text = "儲存";
            this.btn_savePicture.UseVisualStyleBackColor = true;
            this.btn_savePicture.Click += new System.EventHandler(this.Btn_savePicture_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AllowDrop = true;
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(9, 11);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(149, 526);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // selectPictureDialog
            // 
            this.selectPictureDialog.Multiselect = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pb_large);
            this.panel1.Location = new System.Drawing.Point(164, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(545, 364);
            this.panel1.TabIndex = 7;
            // 
            // ThumbnailContextMenu
            // 
            this.ThumbnailContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_switchUp,
            this.ToolStripMenuItem_switchDown,
            this.toolStripSeparator1,
            this.ToolStripMenuItem_DeletPicture});
            this.ThumbnailContextMenu.Name = "ThumbnailContextMenu";
            this.ThumbnailContextMenu.Size = new System.Drawing.Size(181, 98);
            // 
            // ToolStripMenuItem_DeletPicture
            // 
            this.ToolStripMenuItem_DeletPicture.Name = "ToolStripMenuItem_DeletPicture";
            this.ToolStripMenuItem_DeletPicture.Size = new System.Drawing.Size(180, 22);
            this.ToolStripMenuItem_DeletPicture.Text = "刪除圖片";
            this.ToolStripMenuItem_DeletPicture.Click += new System.EventHandler(this.ToolStripMenuItem_DeletPicture_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(177, 6);
            // 
            // ToolStripMenuItem_switchUp
            // 
            this.ToolStripMenuItem_switchUp.Name = "ToolStripMenuItem_switchUp";
            this.ToolStripMenuItem_switchUp.Size = new System.Drawing.Size(180, 22);
            this.ToolStripMenuItem_switchUp.Text = "調整順序-上移一張";
            this.ToolStripMenuItem_switchUp.Click += new System.EventHandler(this.ToolStripMenuItem_switchUp_Click);
            // 
            // ToolStripMenuItem_switchDown
            // 
            this.ToolStripMenuItem_switchDown.Name = "ToolStripMenuItem_switchDown";
            this.ToolStripMenuItem_switchDown.Size = new System.Drawing.Size(180, 22);
            this.ToolStripMenuItem_switchDown.Text = "調整順序-下移一張";
            this.ToolStripMenuItem_switchDown.Click += new System.EventHandler(this.ToolStripMenuItem_switchDown_Click);
            // 
            // PicturesPage
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 541);
            this.Controls.Add(this.btn_savePicture);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_addPicture);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.tb_caption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "PicturesPage";
            this.Text = "PicturesPage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PicturesPage_FormClosing);
            this.Load += new System.EventHandler(this.PicturesPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pb_large)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ThumbnailContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_large;
        private System.Windows.Forms.TextBox tb_caption;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_addPicture;
        private System.Windows.Forms.Button btn_savePicture;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.OpenFileDialog selectPictureDialog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ContextMenuStrip ThumbnailContextMenu;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_DeletPicture;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_switchUp;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_switchDown;
    }
}