﻿using System.Windows.Forms;

namespace MASReportBuilder
{
    partial class Form1
    {
        private void SetDataBindings()
        {
            this.DataBindings.Add(
                "Text", ReportDataSource, "TitleString", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_reportNo.DataBindings.Add(
                "Text", ReportDataSource, "ReportNo", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_companyName.DataBindings.Add(
                "Text", ReportDataSource, "CompanyName", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_companyAddr.DataBindings.Add(
                "Text", ReportDataSource, "CompanyAddr", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_developerName.DataBindings.Add(
                "Text", ReportDataSource, "DeveloperName", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_appCommonName.DataBindings.Add(
                "Text", ReportDataSource, "AppCommonName", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_appId.DataBindings.Add(
                "Text", ReportDataSource, "AppId", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_appSHA1.DataBindings.Add(
                "Text", ReportDataSource, "AppSHA1", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_appVersion.DataBindings.Add(
                "Text", ReportDataSource, "AppVersion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.tb_reportVersion.DataBindings.Add(
                "Text", ReportDataSource, "ReportVersion", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dtp_finishDate.DataBindings.Add(
                "Text", ReportDataSource, "Finishdate", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dtp_startDate.DataBindings.Add(
                "Text", ReportDataSource, "StartDate", true, DataSourceUpdateMode.OnPropertyChanged);

            this.dtp_reportDate.DataBindings.Add(
                "Text", ReportDataSource, "ReportDate", true, DataSourceUpdateMode.OnPropertyChanged);

            this.comboBox1.DataBindings.Add(
                "SelectedIndex", ReportDataSource, "Class", true, DataSourceUpdateMode.OnPropertyChanged);

            this.rb_iOS.DataBindings.Add(
                "Checked", ReportDataSource, "IsiOS", true, DataSourceUpdateMode.OnPropertyChanged);

            this.rb_android.DataBindings.Add(
                "Checked", ReportDataSource, "IsAndroid", true, DataSourceUpdateMode.OnPropertyChanged);
        }
    }
}
