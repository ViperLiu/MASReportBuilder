﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Xceed.Words.NET;

namespace MASReportBuilder
{
    public partial class Form1
    {
        private void Btn_outputDocx_Click(object sender, EventArgs e)
        {
            var fileName = Path.GetFileName(MASData.Report.CurrentOpenedFile);
            fileName = fileName.Replace(".jsonr", "");
            
            //在輸出報告前，讓使用者存檔
            if (MASData.Report.TitleString.StartsWith("*"))
            {
                var r = MessageBox.Show("輸出報告前是否先存檔？", "尚未存檔", MessageBoxButtons.YesNoCancel);
                if (r == DialogResult.Yes)
                {
                    if (File.Exists(MASData.Report.CurrentOpenedFile))
                        SaveFile(MASData.Report.CurrentOpenedFile);
                    else
                        SaveAsNewFile();
                    return;
                }
                else if (r == DialogResult.Cancel)
                {
                    return;
                }
            }
            
            saveDocxDialog.FileName = fileName;
            var result = saveDocxDialog.ShowDialog();
            if (result == DialogResult.Cancel)
                return;

            try
            {
                MASData.Report.BuildReport(saveDocxDialog.FileName);
            }
            catch(Exception exception)
            {
                tb_outputDetail.AppendText(exception.StackTrace + "\r\n");
                return;
            }
            tb_outputDetail.AppendText("[INFO] 輸出完畢\r\n");

            Process.Start(saveDocxDialog.FileName);
        }

        private void Btn_readApplication_Click(object sender, EventArgs e)
        {
            var result = openApplicationDialog.ShowDialog();
            var fileName = openApplicationDialog.FileName;
            if (result == DialogResult.OK)
            {
                ReadApplication(fileName);
            }
        }

        public void ReadApplication(string filePath)
        {
            DocX document;
            var fileName = Path.GetFileName(filePath);
            try
            {
                document = DocX.Load(filePath);
            }
            catch
            {
                tb_outputDetail.AppendText("[ERROR] 無法開啟 " + fileName + "，檔案可能已被其他程式開啟\r\n");
                return;
            }

            //讀取APP資訊
            var appCommonName = document.Tables[1].Rows[0].Cells[1].Paragraphs.First().Text;
            var appVersion = document.Tables[1].Rows[1].Cells[1].Paragraphs.First().Text;
            var appOS = document.Tables[1].Rows[1].Cells[3].Paragraphs.First().Text.ToLower();
            var apkId = document.Tables[2].Rows[1].Cells.Last().Paragraphs.First().Text;
            var appSHA1 = document.Tables[2].Rows[2].Cells.Last().Paragraphs.First().Text;

            //讀取送測單位資訊
            var companyName = document.Tables[0].Rows[0].Cells.Last().Paragraphs.First().Text;
            var companyAddr = document.Tables[0].Rows[2].Cells.Last().Paragraphs.First().Text;

            //顯示
            MASData.Report.AppCommonName = tb_appCommonName.Text = appCommonName;
            MASData.Report.AppVersion = tb_appVersion.Text = appVersion;
            MASData.Report.AppId = tb_appId.Text = apkId;
            MASData.Report.AppSHA1 = tb_appSHA1.Text = appSHA1.Replace(":", "").ToLower();
            MASData.Report.CompanyName = tb_companyName.Text = tb_developerName.Text = companyName;
            MASData.Report.DeveloperName = tb_companyAddr.Text = companyAddr;

            if (MASData.ProjectInfo.OS != appOS)
                throw new Exception(
                    string.Format("申請書內的作業系統({0})與你指定的({1})不符", appOS, MASData.ProjectInfo.OS));

            if (appOS == "android")
                rb_android.Checked = true;
            else
                rb_iOS.Checked = true;
            MASData.Report.AppOS = appOS;


            document.Dispose();

            tb_outputDetail.AppendText("[INFO] 已載入申請書：\r\n" + fileName + "\r\n");
        }

        private void Rb_class1_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_class1.Checked)
                comboBox1.SelectedIndex = 0;
        }

        private void Rb_class2_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_class2.Checked)
                comboBox1.SelectedIndex = 1;
        }

        private void Rb_class3_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_class3.Checked)
                comboBox1.SelectedIndex = 2;
        }
    }
}
