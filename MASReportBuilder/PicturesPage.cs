﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace MASReportBuilder
{
    public partial class PicturesPage : Form
    {
        public List<Picture> selectedPictures = new List<Picture>();
        public Picture CurrentSelectPicture;
        private readonly ushort SubRuleIndex;
        private readonly SubRuleResult Result;

        public PicturesPage(ushort subRuleIndex)
        {
            InitializeComponent();
            pb_large.BorderStyle = BorderStyle.FixedSingle;
            this.SubRuleIndex = subRuleIndex;
            this.Result = MASData.CurrentSelectedRuleResult.SubRuleList[subRuleIndex];
        }

        //重新整理左側縮圖
        private void RefreshThumbnailUI()
        {
            flowLayoutPanel1.Controls.Clear();
            var index = 0;
            Bitmap bm;
            foreach (Picture p in this.Result.Pictures)
            {
                PictureBox pb = new PictureBox();
                if (p.Image != null)
                {
                    bm = ResizeImg(p.Image, maxWidth: 120);
                    pb.Width = bm.Width;
                    pb.Height = bm.Height;
                }
                else
                {
                    bm = (Bitmap)pb.ErrorImage;
                    pb.Width = 120;
                    pb.Height = 120;
                }
                
                pb.Visible = true;
                pb.Image = bm;
                pb.BorderStyle = BorderStyle.FixedSingle;
                pb.Tag = index;
                pb.ContextMenuStrip = ThumbnailContextMenu;
                pb.Click += new EventHandler(Thumbnail_Click);
                this.flowLayoutPanel1.Controls.Add(pb);
                index++;
            }
        }

        private void Btn_addPicture_Click(object sender, EventArgs e)
        {
            SaveCaption();
            selectPictureDialog.InitialDirectory = MASData.ProjectInfo.GetFullPath(ProjectPath.PicturesFolder);
            var result = selectPictureDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                foreach (string p in selectPictureDialog.FileNames)
                {
                    var fileName = Path.GetFileName(p);
                    var newPath = MASData.ProjectInfo.GetFullPath(ProjectPath.PicturesFolder) + "\\" + fileName;
                    if (!File.Exists(newPath))
                        File.Copy(p, newPath);
                    Console.WriteLine(newPath);
                    Picture pic = new Picture(fileName);
                    pic.CreatePicture();
                    this.Result.Pictures.Add(pic);
                }
                RefreshThumbnailUI();
                SelectPicture(this.Result.Pictures.Count - 1);
            }
        }

        private void Thumbnail_Click(object sender, EventArgs e)
        {
            SaveCaption();

            var pb_clicked = (PictureBox)sender;
            var index = (int)pb_clicked.Tag;

            SelectPicture(index);
        }

        private void PicturesPage_Load(object sender, EventArgs e)
        {
            StringBuilder missingFileStr = new StringBuilder();
            var isMissingFile = false;
            foreach(var pic in Result.Pictures)
            {
                try
                {
                    pic.CreatePicture();
                }
                catch
                {
                    isMissingFile = true;
                    missingFileStr.Append(pic.Path + "\r\n");
                }
            }

            if (isMissingFile)
            {
                MessageBox.Show(missingFileStr.ToString(), "找不到圖片");
            }

            RefreshThumbnailUI();
            SelectPicture(0);
        }

        private void ToolStripMenuItem_DeletPicture_Click(object sender, EventArgs e)
        {
            var clickedPictureBox = GetPictureBoxFromToolStripItem(sender);
            var index = (int)clickedPictureBox.Tag;
            this.Result.Pictures.RemoveAt(index);
            flowLayoutPanel1.Controls.Remove(clickedPictureBox);
            RefreshThumbnailUI();

            //刪除圖片list中最尾端的圖片時
            if (index == this.Result.Pictures.Count)
                index--;

            SelectPicture(index);
        }

        //在既定的寬高範圍內，依比例縮放圖片
        private Bitmap ResizeImg(Image imgToResize, int maxWidth = int.MaxValue, int maxHeight = int.MaxValue)
        {
            if (maxWidth <= 0 || maxHeight <= 0)
            {
                throw new ArgumentException("The value of maxWidth and maxHeight must be greater than 0");
            }
            if (imgToResize == null)
            {
                throw new ArgumentNullException("Argument \"imgToResize\" cannot be null");
            }
            var imgRatio = (double)imgToResize.Width / (double)imgToResize.Height;
            var containerRatio = (double)maxWidth / (double)maxHeight;
            var newImageWidth = 0;
            var newImageHeight = 0;

            if (imgRatio < containerRatio)
            {
                newImageHeight = maxHeight;
                newImageWidth = (int)(newImageHeight * imgRatio);
            }
            else
            {
                newImageWidth = maxWidth;
                newImageHeight = (int)(newImageWidth / imgRatio);
            }

            return new Bitmap(imgToResize, newImageWidth, newImageHeight);
        }

        private void Btn_savePicture_Click(object sender, EventArgs e)
        {
            SaveCaption();
            this.Close();
            this.Dispose();
        }

        private void DisplayBigPicture(int picIndex)
        {
            var img = this.Result.Pictures[picIndex];

            var bm = ResizeImg(img.Image, panel1.Width, panel1.Height);

            //make picture align center
            var x = (panel1.Width - bm.Width) / 2;
            var y = (panel1.Height - bm.Height) / 2;
            pb_large.Location = new Point(x, y);

            //resize picturebox
            pb_large.Width = bm.Width;
            pb_large.Height = bm.Height;
            pb_large.Image = bm;
            pb_large.Visible = true;
            pb_large.BorderStyle = BorderStyle.FixedSingle;
        }

        private void DisplayCaption(int picIndex)
        {
            tb_caption.Text = CurrentSelectPicture.Caption;
        }

        private void SelectPicture(int picIndex)
        {
            //沒有圖片可以顯示
            if (this.Result.Pictures.Count == 0 || this.Result.Pictures[picIndex].Image == null)
            {
                pb_large.Image = null;
                tb_caption.Text = "";
                return;
            }

            try
            {
                this.CurrentSelectPicture = this.Result.Pictures[picIndex];
                DisplayBigPicture(picIndex);
                DisplayCaption(picIndex);
            }
            catch (ArgumentOutOfRangeException e)
            {
                MessageBox.Show(e.Message, "找不到圖片");
            }
        }

        private void SaveCaption()
        {
            if (CurrentSelectPicture is null)
                return;

            CurrentSelectPicture.AddCaption(tb_caption.Text);
        }

        private void PicturesPage_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveCaption();
            foreach(var pic in Result.Pictures)
            {
                if (pic.Image == null)
                    continue;
                pic.ReleasePictureResource();
            }
        }

        //將圖片改變順序-上移
        private void ToolStripMenuItem_switchUp_Click(object sender, EventArgs e)
        {
            var pic = GetPictureBoxFromToolStripItem(sender);

            var index = (int)pic.Tag;
            var picList = this.Result.Pictures;

            //第一張無法上移
            if (index == 0)
                return;

            //swap
            var tmp = picList[index];
            picList[index] = picList[index - 1];
            picList[index - 1] = tmp;

            RefreshThumbnailUI();
            SelectPicture(index - 1);
        }

        //將圖片改變順序-下移
        private void ToolStripMenuItem_switchDown_Click(object sender, EventArgs e)
        {
            var pic = GetPictureBoxFromToolStripItem(sender);
            var index = (int)pic.Tag;
            var picList = this.Result.Pictures;

            //最後一張無法下移
            if (index == picList.Count - 1)
                return;

            //swap
            var tmp = picList[index];
            picList[index] = picList[index + 1];
            picList[index + 1] = tmp;

            RefreshThumbnailUI();
            SelectPicture(index + 1);
        }
        
        //取得使用者在哪一個PictureBox點右鍵
        private PictureBox GetPictureBoxFromToolStripItem(object sender)
        {
            var x = sender as ToolStripItem;
            var owner = x.Owner as ContextMenuStrip;

            return owner.SourceControl as PictureBox;
        }
    }
}
