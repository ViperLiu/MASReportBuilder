﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using MASToolBox;
using Newtonsoft.Json;

namespace MASReportBuilder
{
    public partial class Form1 : Form
    {
        public GroupBox[] groups;
        public TextBox[] tb_Detail;
        public Button[] pictureButtons;
        public List<RadioButton> radioButtons;
        private BindingSource ReportDataSource = new BindingSource { DataSource = typeof(Report) };
        private bool SuppressRadioButtonEvent = false;
        public Form1()
        {
            InitializeComponent();
            groups = new GroupBox[7] { gb_subRule1, gb_subRule2, gb_subRule3, gb_subRule4, gb_subRule5, gb_subRule6, gb_subRule7 };
            tb_Detail = new TextBox[7] { detail1, detail2, detail3, detail4, detail5, detail6, detail7 };
            pictureButtons = new Button[7] {
                btn_Picture1, btn_Picture2, btn_Picture3,
                btn_Picture4, btn_Picture5, btn_Picture6, btn_Picture7
            };
            radioButtons = new List<RadioButton> {
                rb_rule1Accept, rb_rule1Fail,
                rb_rule2Accept, rb_rule2Fail,
                rb_rule3Accept, rb_rule3Fail,
                rb_rule4Accept, rb_rule4Fail,
                rb_rule5Accept, rb_rule5Fail,
                rb_rule6Accept, rb_rule6Fail,
                rb_rule7Accept, rb_rule7Fail
            };
            BindEventToRadioButton();
            BindEventToPictureButton();
            openJsonrDialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\saves";
            treeView1.ExpandAll();
            SetDataBindings();
            BuildData();
        }

        private void Btn_PictureButton_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            var subRuleNum = ushort.Parse(btn.Tag as string);

            if (MASData.CurrentSelectRuleNumber == null)
            {
                MessageBox.Show("沒有選取任何檢測項目");
                return;
            }
            PicturesPage picturesPage = new PicturesPage(subRuleNum);
            var result = picturesPage.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                picturesPage.Dispose();
            }
            UpdatePictureBtnText();
        }

        private void Rb_ruleCheckedChanged(object sender, EventArgs e)
        {
            if (this.SuppressRadioButtonEvent)
                return;
            RadioButton x = (RadioButton)sender;
            var rbIndex = radioButtons.IndexOf(x);
            var subRuleNum = rbIndex / 2;
            if (x.Checked)
            {
                var subRuleResult = MASData.CurrentSelectedRuleResult.SubRuleList[subRuleNum];
                var subRuleContent = MASData.CurrentSelectedRuleContent.SubRuleContentsList[subRuleNum];
                if (x.Text == "符合")
                {
                    subRuleResult.Text = subRuleContent.DefaultAcceptText;
                    subRuleResult.Accept();
                }
                else
                {
                    subRuleResult.Text = subRuleContent.DefaultFailText;
                    subRuleResult.Fail();
                }
            }
            DisplayResultText();
        }
        
        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SaveResults();

            string selectedRule = e.Node.Text;

            //not click on the rule node
            if (e.Node.Level == 0)
                return;
            //in case select rule fail
            if (!SelectRule(selectedRule))
                return;
            
            ClearRadioButtons();
            
            //display subrule detail, textbox, radio button...etc
            UpdateSubRuleUI();

            //看檢測項目是否為不須檢測，如不須檢測，則disable符合、不符合、不適用、圖片的button
            UpdateDontTestUI();

            //看檢測項目是否有不適用的選項，如無法不適用，則disable不適用的按鈕
            UpdateNotFitBtn();
            
            //更新底下的顯示文字
            UpdateToolStrip();
            
            //將圖片數量顯示在圖片按鈕上
            UpdatePictureBtnText();

            //output finalText to tb_finalText
            DisplayResultText();

            //更新檢測結果label的文字及顏色
            UpdateResultLabel();
        }

        private void Btn_accept_Click(object sender, EventArgs e)
        {
            if (MASData.CurrentSelectedRuleResult == null)
                return;

            MASData.CurrentSelectedRuleResult.Accept();
            MASData.CurrentSelectedRuleResult.SaveResultText(tb_finalText.Text);
            UpdateTreeNodeBackColor(MASData.CurrentSelectRuleNumber);
            UpdateResultLabel();
            Console.WriteLine(MASData.Report.TitleString);
        }

        private void Btn_fail_Click(object sender, EventArgs e)
        {
            if (MASData.CurrentSelectedRuleResult == null)
                return;

            MASData.CurrentSelectedRuleResult.Fail();
            MASData.CurrentSelectedRuleResult.SaveResultText(tb_finalText.Text);
            UpdateTreeNodeBackColor(MASData.CurrentSelectRuleNumber);
            UpdateResultLabel();
        }

        private void Btn_notFit_Click(object sender, EventArgs e)
        {
            if (MASData.CurrentSelectedRuleResult == null)
                return;

            var ruleResult = MASData.CurrentSelectedRuleResult;

            ClearRadioButtons();

            ruleResult.NotFit();

            //display not fit text to textbox
            DisplayResultText();

            ruleResult.SaveResultText(tb_finalText.Text);
            UpdateTreeNodeBackColor(MASData.CurrentSelectRuleNumber);
            UpdateResultLabel();
        }

        private void Btn_saveNewJsonr_Click(object sender, EventArgs e)
        {
            SaveAsNewFile();
        }

        private void Btn_saveJsonr_Click(object sender, EventArgs e)
        {
            SaveResults();

            //如果檔案存在就直接存檔
            if (File.Exists(MASData.Report.CurrentOpenedFile))
            {
                SaveFile(MASData.Report.CurrentOpenedFile);
                return;
            }

            //檔案不存在就開啟存檔視窗
            SaveAsNewFile();

        }
        
        private void Btn_loadJsonr_Click(object sender, EventArgs e)
        {
            DialogResult result = openJsonrDialog.ShowDialog();
            string file = openJsonrDialog.FileName;
            string extension = Path.GetExtension(file).ToLower();
            if (result == DialogResult.OK)
            {
                if (extension == ".jsonr")
                {
                    LoadFile(file);

                    //更新UI
                    ResetUI();
                }
                else
                {
                    MessageBox.Show("不支援此檔案格式");
                }
                Console.WriteLine(MASData.Report.TitleString);
            }
        }

        private void Btn_newJsonr_Click(object sender, EventArgs e)
        {
            if (MASData.Report.TitleString.StartsWith("*"))
            {
                var result = MessageBox.Show(
                    "是否要儲存目前進度？",
                    "進度未儲存", 
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Warning);
                if (result == DialogResult.Cancel)
                    return;
                if(result == DialogResult.Yes)
                {
                    if (File.Exists(MASData.Report.CurrentOpenedFile))
                        SaveFile(MASData.Report.CurrentOpenedFile);
                    else
                        SaveAsNewFile();
                }

            }
            MASData.Report.CurrentOpenedFile = "*未儲存的報告.jsonr";
            BuildData();
            ResetUI();
            UpdateTreeNodeForClass();
        }
        
        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ResetUI();
            
            if (comboBox1.SelectedIndex != MASData.Report.Class && 
                Properties.Settings.Default.ShowClassChangeWarning == true)
            {
                MessageBoxManager.Cancel = "是，不再提醒";
                MessageBoxManager.Register();
                var result = MessageBox.Show(
                    "更改檢測分類，將會使某些測項被標記為\"不須檢測\"，且原先的檢測結果將會遺失，確定要更改嗎？",
                    "警告",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Warning
                    );
                MessageBoxManager.Unregister();
                if (result == DialogResult.No)
                {
                    comboBox1.SelectedIndex = MASData.Report.Class;
                    return;
                }
                else if(result == DialogResult.Cancel)
                {
                    Properties.Settings.Default.ShowClassChangeWarning = false;
                    Properties.Settings.Default.Save();
                }
            }

            SetClass(comboBox1.SelectedIndex);
            
        }

        private void Btn_PassCondition_Click(object sender, EventArgs e)
        {
            if (MASData.CurrentSelectRuleNumber == null)
                return;
            PassConditionWindow ConditionWindow = new PassConditionWindow();
            ConditionWindow.ShowDialog();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void Btn_toolBox_Click(object sender, EventArgs e)
        {
            MASToolBox.Form1 toolbox = new MASToolBox.Form1(JsonConvert.SerializeObject(MASData.ProjectInfo));
            toolbox.Show();
        }
    }
}
